// Generated on 2013-07-09 using generator-angular 0.3.0
'use strict';

var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

function parent(child) {
    return child.substring(0, child.lastIndexOf('/'));
}

module.exports = function (grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    var serverPort = grunt.option('serverPort') || 9005;
    var hostname = grunt.option('hostname') || '0.0.0.0';

    var proxySnippet = require('grunt-connect-proxy/lib/utils').proxyRequest;

    grunt.initConfig({
        connect: {
            proxies: [
                {
                    context: '/api',
                    host: 'localhost',
                    port: 9002,
                    https: false,
                    changeOrigin: true
                }
            ],
            options: {
                port: serverPort,
                hostname: hostname
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            proxySnippet,
                            mountFolder(connect, '.')
                        ];
                    }
                }
            }
        },
        open: {
            server: {
                url: 'http://localhost:<%= connect.options.port %>'
            }
        }
    });

    grunt.registerTask('server', function () {
        grunt.task.run([
            'open',
            'configureProxies',
            'connect:livereload:keepalive'
        ]);
    });

    grunt.registerTask('default', 'server');
};
