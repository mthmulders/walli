"use strict";

/* jasmine specs for the user service. */
describe('The userService', function () {
    beforeEach(module('walli'));

    it("should get all the users", inject(function ($httpBackend, userService) {
        $httpBackend.when('GET', '/api/users?format=json').respond(stubs.services.users, { 'Content-type': 'application/json' });

        var service = userService.query();
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.users)).toBeTruthy();
        expect(service.length).toBe(2);
    }));

    it("should get a specific user", inject(function ($httpBackend, userService) {
        $httpBackend.when('GET', '/api/users/1?format=json').respond(stubs.services.users[0], { 'Content-type': 'application/json' });

        var service = userService.get({id:1});
        $httpBackend.flush();                                                                     

        expect(angular.equals(service, stubs.services.users[0])).toBeTruthy();
    }));

    it("should create a user", inject(function ($httpBackend, userService) {
        $httpBackend.when('POST', '/api/users?format=json').respond(function(method, url, data, headers){
            var user = angular.fromJson(data);
            user.id = 3;
            stubs.services.users.push(user);
            return [200, {}, {}];
        });

        var service = userService.save({"username": "john", "firstName": "John", "lastName": "Doe", "email": "john@doe.org", "password": "password", "admin": false});
        $httpBackend.flush();

        expect(stubs.services.users.length).toBe(3);
    }));

    it("should update a user", inject(function ($httpBackend, userService) {
        $httpBackend.when('PUT', '/api/users/3?format=json').respond(function(method, url, data, headers){
            var user = angular.fromJson(data);
            user.id = 3;
            stubs.services.users[2] = user;
            return [200, {}, {}];
        });

        var service = userService.update({"id": 3, "username": "john", "firstName": "John", "lastName": "Doe", "email": "john@doe.org", "password": "password", "admin": true});
        $httpBackend.flush();

        expect(stubs.services.users.length).toBe(3);
        expect(stubs.services.users[2].admin).toBeTruthy();
    }));

    it("should delete a user", inject(function ($httpBackend, userService) {
        $httpBackend.when('DELETE', '/api/users/3?format=json').respond(function(method, url, data, headers){
            stubs.services.users.pop();
            return [200, {}, {}];
        });

        var service = userService.destroy({"id": 3});
        $httpBackend.flush();

        expect(stubs.services.users.length).toBe(2);
    }));
});
