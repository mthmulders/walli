"use strict";

/* jasmine specs for the project service. */
describe('The projectService', function () {
    beforeEach(module('walli'));

    it("should get all the projects", inject(function ($httpBackend, projectService) {
        $httpBackend.when('GET', '/api/projects?format=json').respond(stubs.services.projects, { 'Content-type': 'application/json' });

        var service = projectService.query();
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.projects)).toBeTruthy();
        expect(service.length).toBe(1);
    }));
    it("should get a specific project", inject(function ($httpBackend, projectService) {
        $httpBackend.when('GET', '/api/projects/1?format=json').respond(stubs.services.projects[0], { 'Content-type': 'application/json' });

        var service = projectService.get({id:1});
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.projects[0])).toBeTruthy();
    }));
    it("should create a project", inject(function ($httpBackend, projectService) {
        $httpBackend.when('POST', '/api/projects?format=json').respond(function(method, url, data, headers){
            var project = angular.fromJson(data);
            project.id = 2;
            stubs.services.projects.push(project);
            return [200, {}, {}];
        });

        var service = projectService.save({"name": "test", "displayName": "Test", "infos": [
            {"id": 1, "name": "bitbucket", "value": "walli/test"},
            {"id": 3, "name": "sonar", "value": "2"},
            {"id": 2, "name": "jenkins", "value": "test"}
        ]});
        $httpBackend.flush();

        expect(stubs.services.projects.length).toBe(2);
    }));
    it("should update a project", inject(function ($httpBackend, projectService) {
        $httpBackend.when('PUT', '/api/projects/2?format=json').respond(function(method, url, data, headers){
            var project = angular.fromJson(data);
            project.id = 2;
            stubs.services.projects[1] = project;
            return [200, {}, {}];
        });

        var service = projectService.update({"id": 2, "name": "test", "displayName": "Updated Test", "infos": [
            {"id": 1, "name": "bitbucket", "value": "walli/test"},
            {"id": 3, "name": "sonar", "value": "2"},
            {"id": 2, "name": "jenkins", "value": "test"}
        ]});
        $httpBackend.flush();

        expect(stubs.services.projects.length).toBe(2);
        expect(stubs.services.projects[1].displayName).toBe('Updated Test');
    }));
    it("should delete a project", inject(function ($httpBackend, projectService) {
        $httpBackend.when('DELETE', '/api/projects/2?format=json').respond(function(method, url, data, headers){
            stubs.services.projects.pop();
            return [200, {}, {}];
        });

        var service = projectService.destroy({"id": 2});
        $httpBackend.flush();

        expect(stubs.services.projects.length).toBe(1);
    }));

});
