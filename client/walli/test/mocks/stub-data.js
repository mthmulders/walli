var stubs = {
    services: {
        keepalive: { "alive": true},
        projects: [
            {"id": 1, "name": "walli", "displayName": "Walli", "infos": [
                {"id": 1, "name": "bitbucket", "value": "walli/walli"},
                {"id": 3, "name": "sonar", "value": "1"},
                {"id": 2, "name": "jenkins", "value": "walli"}
            ]}
        ],
        sources: [
            {"id": 1, "name": "bitbucket", "url": "https://bitbucket.org", "authentication": "basic", "username": "walli", "password": "walli"},
            {"id": 2, "name": "sonar", "url": "http://sonar/api", "authentication": "none"},
            {"id": 3, "name": "jenkins", "url": "http://jenkins/job", "authentication": "none"}
        ],
        users: [
            {"id": 1, "username": "admin", "firstName": "Mischa", "lastName": "Dasberg", "email": "admin@walli.org", "password": "password", "admin": true},
            {"id": 2, "username": "walli", "firstName": "Walli", "lastName": "Dashboard", "email": "walli@walli.org", "password": "password", "admin": false}
        ],
        proxy: {"name": "proxy"},
        authentication: {"status": true},
        configurations: [
            {"id": 1, "name": "refreshTimeout", "value": "3000"},
            {"id": 2, "name": "buildBy", "value": "walli"}
        ]

    }
};