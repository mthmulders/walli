"use strict";

/* Project service. */

angular.module('walli').factory('projectService', ['$resource', function ($resource) {
    return $resource('/api/projects/:id', {
        format: 'json',
        id: '@id'}, {
        query: {
            method: 'GET',
            isArray: true
        },
        update: {method: 'PUT'},
        save: {method: 'POST'},
        destroy: {method: 'DELETE'}
    });
}]);