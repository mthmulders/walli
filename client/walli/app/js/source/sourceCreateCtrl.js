"use strict";

/* Controller that handles the creation of a source. */

/* global _ */
angular.module('walli').controller('sourceCreateCtrl', [
    '$scope',
    '$q',
    '$location',
    'sourceService',
    'modelFactory', function ($scope, $q, $location, sourceService, modelFactory) {
        $scope.source = {
            authentication: 'none'
        };

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        /* Indicate the source name is unique. */
        $scope.uniqueSource = function () {
            var defer = $q.defer();
            sourceService.query({}, function (sources) {
                var found = _.find(sources, function (s) {
                    return s.name === $scope.source.name;
                });
                defer.resolve(found === undefined);
            });
            return defer.promise;
        }

        $scope.authenticationOptions = [
            {value: 'none', label: 'No authentication'},
            {value: 'rsa', label: 'Oauth using RSA-SHA1'},
            {value: 'hmac', label: 'Oauth using HMAC-SHA1'},
            {value: 'basic', label: 'Basic authentication'}
        ];

        /* Get the label for the authentication matching the given key. */
        $scope.authenticationLabel = function (key) {
            var label;
            angular.forEach($scope.authenticationOptions, function (option) {
                if (option.value === key) {
                    label = option.label;
                }
            });
            return label;
        }

        /* Save the source. */
        $scope.save = function () {
            sourceService.save($scope.source, function () {
                $location.path('/settings');
            });
        }
    }]);