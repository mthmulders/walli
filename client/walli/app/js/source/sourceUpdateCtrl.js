"use strict";

/* Controller that handles the updating of a source. */

angular.module('walli').controller('sourceUpdateCtrl', [
    '$scope',
    '$route',
    '$routeParams',
    '$location',
    'sourceService',
    'modelFactory', function ($scope, $route, $routeParams, $location, sourceService, modelFactory) {
        var self = this;

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        $scope.source = sourceService.get({id: $routeParams.sourceId}, function (source) {
            source.consumerSecret = "unchanged";
            source.oauthToken = "unchanged";
            source.oauthSecret = "unchanged";
            source.password = "unchanged";
        });

        $scope.authenticationOptions = [
            {value: 'none', label: 'No authentication'},
            {value: 'rsa', label: 'Oauth using RSA-SHA1'},
            {value: 'hmac', label: 'Oauth using HMAC-SHA1'},
            {value: 'basic', label: 'Basic authentication'}
        ];

        /* Get the label for the authentication matching the given key. */
        $scope.authenticationLabel = function (key) {
            var label;
            angular.forEach($scope.authenticationOptions, function (option) {
                if (option.value === key) {
                    label = option.label;
                }
            });
            return label;
        }

        /* Indicate if the source is clean. */
        $scope.isClean = function () {
            return angular.equals(self.original, $scope.source);
        }

        /* Deletes the source. */
        $scope.destroy = function () {
            this.source.$destroy({id: $routeParams.sourceId}, function () {
                $location.path('/settings');
            });
        };

        /* Updates the source. */
        $scope.save = function () {
            this.source.$update({id: $routeParams.sourceId}, function () {
                $location.path('/settings');
            });
        };
    }]);