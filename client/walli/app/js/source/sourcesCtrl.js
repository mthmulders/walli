"use strict";

/* Controller that gets all the sources. */

angular.module('walli').controller('sourcesCtrl', [
    '$scope',
    'sourceService',
    'modelFactory', function ($scope, sourceService, modelFactory) {
        $scope.sources = sourceService.query({}, function (sources) {

            var sourcesConfig = modelFactory.createDatatableConfig()
                .addColumn('name', 'Name', '', '{{row.name}}', true)
                .addColumn('', 'Opts', 'icon', '<a href="#/sources/{{row.id}}"><i class="icon-pencil"></i></a>', true)
                .addRows($scope.sources);

            $scope.sourcesConfig = sourcesConfig;
        }, function (response) {
        });
    }]);