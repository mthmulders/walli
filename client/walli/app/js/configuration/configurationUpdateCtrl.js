"use strict";

/* Controller that handles the updating of a configuration. */

angular.module('walli').controller('configurationUpdateCtrl', [
    '$scope',
    '$route',
    '$routeParams',
    '$location',
    'configurationService',
    'modelFactory', function ($scope, $route, $routeParams, $location, configurationService, modelFactory) {
        var self = this;

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);


        $scope.configuration = configurationService.get({id: $routeParams.configurationId});

        /* Indicate if the configuration is clean. */
        $scope.isClean = function () {
            return angular.equals(self.original, $scope.configuration);
        }

        /* Deletes the configuration. */
        $scope.destroy = function () {
            this.configuration.$destroy({id: $routeParams.configurationId}, function () {
                $location.path('/settings');
            });
        };

        /* Updates the configuration. */
        $scope.save = function () {
            this.configuration.$update({id: $routeParams.configurationId}, function () {
                $location.path('/settings');
            });
        };
    }]);
