"use strict";

/* Controller that handles connecting with and processing data from jenkins. */

/* global _, $ */
angular.module('walli').controller('jenkinsCtrl', [
    '$scope',
    '$routeParams',
    '$q',
    'projectService',
    'proxyService',
    'userPreferenceService',
    'modelFactory',
    'utils', function ($scope, $routeParams, $q, projectService, proxyService, userPreferenceService, modelFactory, utils) {
        $scope.online = true;
        $scope.executionTimesIdentifier = 'executionTimes';

        var tooltip = document.createElement('div'),
            leftOffset = -($('html').css('padding-left').replace('px', '') + $('body').css('margin-left').replace('px', '')),
            topOffset = -32;
        tooltip.className = 'ex-tooltip';
        document.body.appendChild(tooltip);

        $scope.$on('project', function (event, project) {
            var source = _.filter(project.infos, function (info) {
                return info.name === 'jenkins'
            })[0];

            if (source !== undefined && !_.isEmpty(source.value)) {
                var jenkinsJobName = source.value;

                fetch(jenkinsJobName, false);
                $scope.$on('refresh', function () {
                    fetch(jenkinsJobName, true);
                });
            }
        });

        /**
         * Fetch the actual data.
         * @param jenkinsJobName The jenkins job name.
         * @param refresh Indicator refresh.
         */
        function fetch(jenkinsJobName, refresh) {
            jobData(jenkinsJobName, refresh).then(function (jobs) {
                jenkinsJobBuildStatusesData(jobs.name, jobs.builds, refresh).then(function (buildStatuses) {
                    var subset = angular.copy(buildStatuses).splice(0, 20);

                    var executionTimesData = modelFactory.createXChartData().yScale('linear');
                    angular.forEach(subset, function (build) {
                        executionTimesData.addDataEntry(build.title, (build.duration / 60000));
                    });

                    $scope.executionTimesChartOptions = modelFactory.createXChartOptions()
                        .xMin(1)
                        .xMax(subset.length)
                        .paddingLeft(85)
                        .paddingRight(10)
                        .paddingTop(10)
                        .axisPaddingLeft(5)
                        .tickHintY(6)
                        .dataFormatX(function (x) {
                            return x;
                        })
                        .tickFormatY(function (y) {
                            return utils.duration(y);
                        })
                        .mouseover(function (d, i) {
                            var pos = $(this).offset();
                            $(tooltip).text("Build #" + d.x + " took " + utils.duration(d.y))
                                .css({top: topOffset + pos.top, left: pos.left + leftOffset})
                                .show();
                        })
                        .mouseout(function (x) {
                            $(tooltip).hide();
                        })
                        .options;

                    $scope.executionTimesData = executionTimesData.data;

                    var buildStatusesConfig = modelFactory.createDatatableConfig()
                        .addColumn('value', '', 'icon', '<state-icon config=\'config.stateConfig\' state=\'row.status\'/>', true)
                        .addColumn('', 'Build', '', '#{{row.title}}', true)
                        .addColumn('timestamp', 'Build date', '', '{{row.timestamp | date:\'dd MMM `yy - HH:mm:ss\'}}', true)
                        .addRows(angular.copy(buildStatuses).splice(0, 6));

                    buildStatusesConfig.stateConfig = modelFactory.createStateConfig()
                        .addState('SUCCESS', 'icon-ok status-success')
                        .addState('ABORTED', 'icon-warning-sign status-warning')
                        .addState('UNSTABLE', 'icon-remove status-error')
                        .addState('FAILURE', 'icon-remove status-error', true);


                    $scope.buildStatusesConfig = buildStatusesConfig;
                    $scope.online = true;
                }, function (reason) {
                    $scope.online = false;
                });
            }, function (reason) {
                $scope.online = false;
            });
        }

        /**
         * Get the jenkins job data for the job matching the given jenkins job name.
         * @param jobName The jenkins job name.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function jobData(jobName, refresh) {
            var deferred = $q.defer();

            var success = function (data) {
                var job = data;
                deferred.resolve({name: job.name, builds: job.builds});
            };

            var error = function (response) {
                deferred.reject({failed: true, status: response.status});
            };

            if (refresh) {
                proxyService.queryRefresh({
                    source: 'jenkins',
                    query: jobName + '/api/json'
                }, success, error);
            } else {
                proxyService.query({
                    source: 'jenkins',
                    query: jobName + '/api/json'
                },success, error);
            }

            return deferred.promise;
        }

        /**
         * Get the jenkins job build status data for the job matching the given name and the build matching the given build number.
         * @param jobName The job name.
         * @param buildNumber The build number.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function jenkinsJobBuildStatusData(jobName, buildNumber, refresh) {
            var deferred = $q.defer();

            var success = function (data) {
                deferred.resolve({title: buildNumber, status: data.result, duration: data.duration, timestamp: data.timestamp})
            };

            var error =  function (response) {
                deferred.reject({failed: true, status: response.status});
            };

            if (refresh) {
                proxyService.queryRefresh({
                    source: 'jenkins',
                    query: jobName + "/" + buildNumber + '/api/json'
                }, success, error);
            } else {
                proxyService.query({
                    source: 'jenkins',
                    query: jobName + "/" + buildNumber + '/api/json'
                }, success, error);
            }

            return deferred.promise;
        }

        /**
         * Get the jenkins job build statuses data for the job matching the given name and the given builds.
         * @param jobName The job name.
         * @param builds The builds.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function jenkinsJobBuildStatusesData(jobName, builds, refresh) {
            var deferred = $q.defer();
            var buildStatuses = [];

            var sortedAndTrimmedBuilds = _.chain(builds)
                .sortBy(function (build) {
                    return build.number;
                })
                .reverse().value();

            angular.forEach(sortedAndTrimmedBuilds, function (build) {
                buildStatuses.push(jenkinsJobBuildStatusData(jobName, build.number, refresh));
            });

            $q.all(buildStatuses)
                .then(function (data) {
                    deferred.resolve(data);
                }, function (response) {
                    deferred.reject();
                });
            return deferred.promise;
        }
    }]);