"use strict";

/* Controller that handles the updating of a user. */

angular.module('walli').controller('userEditCtrl', [
    '$scope',
    '$route',
    '$routeParams',
    '$location',
    'userService',
    'modelFactory', function ($scope, $route, $routeParams, $location, userService, modelFactory) {
        var self = this;

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        $scope.user = userService.get({id: $routeParams.userId}, function (user) {
            user.password = "unchanged";
        });

        /* Indicate if the user is clean. */
        $scope.isClean = function () {
            return angular.equals(self.original, $scope.user);
        }

        /* Deletes the user. */
        $scope.destroy = function () {
            this.user.$destroy({id: $routeParams.userId}, function () {
                $location.path('/settings');
            });
        };

        /* Updates the user. */
        $scope.save = function () {
            this.user.$update({id: $routeParams.userId}, function () {
                $location.path('/settings');
            });
        };
    }]);
