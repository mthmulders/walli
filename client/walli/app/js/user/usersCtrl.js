"use strict";

/* Controller that gets all the users. */

angular.module('walli').controller('usersCtrl', [
    '$scope',
    'userService',
    'modelFactory', function ($scope, userService, modelFactory) {
        $scope.users = userService.query({}, function (users) {

            var usersConfig = modelFactory.createDatatableConfig()
                .addColumn('username', 'Username', '', '{{row.username}}', true)
                .addColumn('', 'Opts', 'icon', '<a href="#/users/{{row.id}}"><i class="icon-pencil"></i></a>', true)
                .addRows($scope.users);

            $scope.usersConfig = usersConfig;
        }, function (response) {
        });
    }]);