"use strict";

/* User service. */

angular.module('walli').factory('userService', ['$resource', function ($resource) {
    return $resource('/api/users/:id', {
        format: 'json',
        id: '@id'}, {
        query: {
            method: 'GET',
            isArray: true
        },
        update: {method: 'PUT'},
        save: {method: 'POST'},
        destroy: {method: 'DELETE'}
    });
}]);