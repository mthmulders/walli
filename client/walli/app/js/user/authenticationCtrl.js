"use strict";

/* Controller that handles the authentication of walli. */

angular.module('walli').controller('authenticationCtrl', [
    '$scope',
    '$rootScope',
    'authenticationService',
    '$location',
    '$route', function ($scope, $rootScope, authenticationService, $location, $route) {
        /* Login to walli. */
        $scope.login = function () {
            authenticationService.login($scope.user, function () {
                $location.path('/settings');
            }, function (response) {
                $scope.message = 'Incorrect credentials';
            })
        };

        /* Logout of walli. */
        $scope.logout = function () {
            authenticationService.logout({}, function () {
                $route.reload();
            });
        };

        authenticationService.loggedIn({}, function (response) {
            $scope.loggedIn = response.status;
        });
    }]);