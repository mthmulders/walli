"use strict";

/* Walli controller. */

/* global _, $ */
angular.module('walli').controller('walliCtrl', [
    '$scope',
    'notificationService',
    'configurationService',
    'versionService', function ($scope, notificationService, configurationService, versionService) {
        $scope.notificationService = notificationService;

        $scope.version = versionService.version;

        /* remove the notification from the cache when the notification is closed. */
        $scope.closeNotification = function (index) {
            notificationService.remove(index);
        };

        /* hide the navigation collapse. */
        $scope.hideNavCollapse = function () {
            $('.nav-collapse').collapse('hide');
        };

        $scope.configurations = configurationService.query({});

        $scope.configuration = function (name) {
            var config;
            angular.forEach($scope.configurations, function (configuration) {
                if (configuration.name === name) {
                    config = configuration.value;
                }
            });
            return config;
        };

        $scope.$on('project', function (event, project) {
            $scope.project = project;

            $scope.hasSource = function (source) {
                var s = _.find($scope.project.infos, function (info) {
                    return info.name === source;
                });
                return angular.isDefined(s) && s.value !== '';
            }
        });
    }]);