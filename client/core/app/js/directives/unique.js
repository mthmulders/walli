"use strict";

/* The unique directive can be used for checking uniqueness of a field. */

angular.module('core').directive('unique', function () {
    return {
        restrict: 'A',
        replace: false,
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            element.bind("blur", function () {
                scope.$eval(attrs.unique).then(function (result) {
                    ctrl.$setValidity('unique', result);
                });
            });
        }
    };
});