"use strict";

/* The modal directive. */

/* global $ */
angular.module('core').directive('modal', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var el = $(attrs.modal);
                el.modal(el.css('display') === 'none' ? 'show' : 'hide');
            });
        }
    };
});