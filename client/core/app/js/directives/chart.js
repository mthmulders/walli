"use strict";

/* The chart directive wraps the xchart library. */

/* global xChart, _ */
angular.module('core').directive('chart', function () {
        return {
            restrict: 'E',
            scope: {
                identifier: '=',
                data: "=",
                options: "="
            },
            templateUrl: 'partials/chart.html',
            replace: true,
            link: function (scope, element, attrs) {
                var type = angular.isDefined(attrs.type) ? attrs.type : "line-dotted";

                scope.$watch('data', function (data) {
                    if (data !== undefined) {
                        new xChart(type, scope.data, '#' + attrs.id, scope.options);
                    }
                });
            }
        }
    }
);