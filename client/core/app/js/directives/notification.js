"use strict";

/* The notification directive. */

/* jshint camelcase: false */
/* global $, notificationService, gritter */
angular.module('core').directive('notification', function () {
    return {
        restrict: 'E',
        scope: {
            key: '=',
            title: '=',
            text: '=',
            sticky: '=',
            time: '=',
            className: '='
        },
        link: function (scope) {
            scope.key = angular.isDefined(scope.key) ? scope.key : undefined;

            var config = {
                key: scope.key,
                title: (angular.isDefined(scope.title) ? scope.title : undefined),
                text: (angular.isDefined(scope.text) ? scope.text : undefined),
                sticky: (angular.isDefined(scope.sticky) ? scope.sticky : false),
                time: (angular.isDefined(scope.time) ? scope.time : ''),
                class_name: (angular.isDefined(scope.className) ? scope.className : undefined),
                after_close: scope.afterClose
            };

            $.gritter.add(config);
        },
        controller: 'notificationCtrl'
    };
});


angular.module('core').controller('notificationCtrl', ['$scope', 'notificationService', function ($scope, notificationService) {
    $scope.afterClose = function (event) {
        notificationService.removeNotification($scope.key);
    }
}]);