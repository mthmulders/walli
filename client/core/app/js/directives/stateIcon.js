"use strict";

/* The state icon directive. */

angular.module('core').directive('stateIcon', function () {
    return {
        restrict: 'E',
        scope: {
            config: '=config',
            state: '=state'
        },
        replace: true,
        templateUrl: 'partials/stateIcon.html',
        link: function (scope) {
            scope.matchingClasses = scope.config.default.classes;

            scope.$watch('state', function () {
                angular.forEach(scope.config.states, function (s) {
                    if (s.key === scope.state) {
                        scope.matchingClasses = s.classes;
                    }
                });
            });
        }
    };
});