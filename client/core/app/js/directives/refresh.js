"use strict";

/* Refresh directive takes care of refreshing some data by emitting an event to the parent controller. */

/* global $timeout */
angular.module('core').directive('refresh', ['$timeout', function ($timeout) {
    return {
        restrict: 'EA',
        replace: false,
        scope: true,
        link: function ($scope, $element, $attrs) {
            $scope.$on('$destroy', function () {
                $timeout.cancel($scope.timeoutId)
            });

            $scope.$watch($attrs.refresh, function(val) {
                if (angular.isDefined(val)) {
                    var tick = function () {
                        $scope.$emit('refresh');
                        $scope.timeoutId = $timeout(function () {
                            tick();
                            $scope.$apply();
                        }, val);
                    };

                    tick();
                }
            });
        }
    };
}]);