"use strict";

/* Utils provides utility methods. */

angular.module('core').
    factory('utils', function () {
        return {
            duration: function (d) {
                var duration = new Date(d * 60000);
                var minutes = duration.getMinutes();
                var seconds = duration.getSeconds();

                var result;
                if (minutes > 0) {
                    result = minutes + " min ";
                    result = result + (seconds >= 10 ? seconds : "0" + seconds);
                    result = result + " sec."
                } else {
                    result = duration.getSeconds() + " sec.";
                }
                return result;
            },
            dayTimestamp: function (t) {
                var date = new Date(t);
                return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()).getTime()
            },
            date: function(t) {
                var duration = new Date(t);
                var day = duration.getDate() ;
                var month = duration.getMonth() + 1;
                return (month <= 9 ? '0' + month : month) + '-' + (day <= 9 ? '0' + day : day);
            },
            fullDate: function(t) {
                var duration = new Date(t);
                var day = duration.getDate();
                var month = duration.getMonth() + 1;
                var year = duration.getFullYear();
                return (month <= 9 ? '0' + month : month) + '-' + (day <= 9 ? '0' + day : day) + "-" + year;
            }
        };
    });