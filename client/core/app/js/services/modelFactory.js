"use strict";

/* The modelFactory service.*/

angular.module('core').factory('modelFactory', function () {
    /**
     * Datatable config object.
     * @constructor
     */
    function DatatableConfig() {
        this.headers = [];
        this.columns = [];
        this.rows = [];

        /**
         * Add a Datatable column object with the given parameters.
         * @param key String the key.
         * @param description String the description.
         * @param className String the class name.
         * @param template String the template.
         * @param addAsHeader Boolean indicator add as header.
         * @param colspan Number the colspan.
         * @returns {*}
         */
        this.addColumn = function (key, description, className, template, addAsHeader, colspan) {
            if (addAsHeader) {
                this.headers.push(new DatatableHeader(description, colspan ? colspan : 1));
            }
            this.columns.push(new DatatableColumn(key, description, className, template));
            return this;
        }

        /**
         * Adds a Datatable row object.
         * @param row Object the row object.
         * @returns {*}
         */
        this.addRow = function (row) {
            this.rows.push(row);
            return this;
        }

        /**
         * Adds the Datatable row objects.
         * @param rows Object[] the row objects.
         * @returns {*}
         */
        this.addRows = function (rows) {
            for (var i = 0; i < rows.length; i++) {
                this.rows.push(rows[i]);
            }
            return this;
        }
    }

    /**
     * Datatable header object.
     * @param description String the description.
     * @param colspan Number the colspan.
     * @constructor
     */
    function DatatableHeader(description, colspan) {
        this.description = description;
        this.colspan = colspan;
    }

    /**
     * Datatable column pbject.
     * @param key String the key.
     * @param description String the description.
     * @param className String class name.
     * @param template String template.
     * @constructor
     */
    function DatatableColumn(key, description, className, template) {
        this.key = key;
        this.description = description;
        this.className = className;
        this.template = template;
    }

    /**
     * State config object.
     * @constructor
     */
    function StateConfig() {
        this.default = undefined;
        this.states = [];

        /**
         * Add a state object.
         * @param key String the key.
         * @param classes String the classes.
         * @param isDefault Boolean indicator is default.
         * @returns {*}
         */
        this.addState = function (key, classes, isDefault) {
            var state = new State(key, classes);
            if (isDefault) {
                this.default = state;
            }
            this.states.push(state);
            return this;
        }
    }

    /**
     * State object.
     * @param key String the key.
     * @param backgroundColor String the background color.
     * @constructor
     */
    function State(key, classes) {
        this.key = key;
        this.classes = classes;
    }

    /**
     * Create a Datatable config instance.
     * @returns {DatatableConfig}
     */
    var createDatatableConfig = function () {
        return new DatatableConfig();
    }

    /**
     * Creates a State config instance.
     * @returns {StateConfig}
     */
    var createStateConfig = function () {
        return new StateConfig();
    }

    /**
     * XChart options object.
     * @constructor
     */
    function XChartOptions() {
        /**
         * The options.
         * @type {{}}
         */
        this.options = {}

        /**
         * Sets the mouseover function
         * @param func Function callback behavior for a user mousing over a data point.
         * @returns {*}
         */
        this.mouseover = function (func) {
            this.options.mouseover = func;
            return this;
        };
        /**
         * Sets mouseout.
         * @param func Function callback behavior for a user mousing off a data point.
         * @returns {*}
         */
        this.mouseout = function (func) {
            this.options.mouseout = func;
            return this;
        };
        /**
         * Sets click.
         * @param func Function callback behavior for a user clicking a data point.
         * @returns {*}
         */
        this.click = function (func) {
            this.options.click = func;
            return this;
        };
        /**
         * Sets axis padding top.
         * @param padding Number amount of space between the top of the chart and the top value on the y-scale.
         * @returns {*}
         */
        this.axisPaddingTop = function (padding) {
            this.options.axisPaddingTop = padding;
            return this;
        };
        /**
         * Sets axis padding right.
         * @param padding Number amount of space between the right side of the chart and the highest value on the
         * x-scale.
         * @returns {*}
         */
        this.axisPaddingRight = function (padding) {
            this.options.axisPaddingRight = padding;
            return this;
        };
        /**
         * Sets axis padding bottom.
         * @param padding Number amount of space between the bottom of the chart and the lowest value on the
         * y-scale.
         * @returns {*}
         */
        this.axisPaddingBottom = function (padding) {
            this.options.axisPaddingBottom = padding;
            return this;
        };
        /**
         * Sets axis padding left.
         * @param padding Number amount of space between the left side of the chart and the lowest value on the
         * x-scale.
         * @returns {*}
         */
        this.axisPaddingLeft = function (padding) {
            this.options.axisPaddingLeft = padding;
            return this;
        };
        /**
         * Sets x min.
         * @param min Number minimum allowed value on the xScale. If null, uses the data's min value, logically
         * padded for aesthetics. Does not affect ordinal scales. May be overrided using the setData method with
         * the xMin data format key.
         * @returns {*}
         */
        this.xMin = function (min) {
            this.options.xMin = min;
            return this;
        };
        /**
         * Sets x max
         * @param max Number maximum allowed value on the xScale. If null, uses the data's max value, logically
         * padded for aesthetics. Does not affect ordinal scales. May be overrided using the setData method with
         * the xMax data format key.
         * @returns {*}
         */
        this.xMax = function (max) {
            this.options.xMax = max;
            return this;
        };
        /**
         * Sets y min
         * @param min Number minimum allowed value on the yScale. If null, uses the data's min value, logically
         * padded for aesthetics. Does not affect ordinal scales. May be overrided using the setData method with
         * the yMin data format key.
         * @returns {*}
         */
        this.yMin = function (min) {
            this.options.yMin = min;
            return this;
        };
        /**
         * Sets y max.
         * @param max Number maximum allowed value on the yScale. If null, uses the data's max value, logically
         * padded for aesthetics. Does not affect ordinal scales. May be overrided using the setData method with
         * the yMax data format key.
         * @returns {*}
         */
        this.yMax = function (max) {
            this.options.yMax = max;
            return this;
        };
        /**
         * Sets padding top.
         * @param padding Number amount of space from the top edge of the svg element to the beginning of the
         * axisPaddingTop.
         * @returns {*}
         */
        this.paddingTop = function (padding) {
            this.options.paddingTop = padding;
            return this;
        };
        /**
         * Sets padding right.
         * @param padding Number amount of space from the right edge of the svg element to the beginning of
         * the axisPaddingRight.
         * @returns {*}
         */
        this.paddingRight = function (padding) {
            this.options.paddingRight = padding;
            return this;
        };
        /**
         * Sets padding bottom.
         * @param padding Number allows space for the x-axis scale. Controls the amount of space from the bottom
         * edge of the svg element to the beginning of the
         * @returns {*}
         */
        this.paddingBottom = function (padding) {
            this.options.paddingBottom = padding;
            return this;
        };

        /**
         * Sets padding left.
         * @type padding Number allows space for the y-axis scale. Amount of space from the left edge of the svg
         * element to the beginning of the axisPaddingLeft.
         */
        this.paddingLeft = function (padding) {
            this.options.paddingLeft = padding;
            return this;
        };
        /**
         * Sets tick hint x.
         * @param hint Number the amount of ticks that you would like to have displayed on the x-axis
         * @returns {*}
         */
        this.tickHintX = function (hint) {
            this.options.tickHintX = hint;
            return this;
        };
        /**
         * Sets tick format x.
         * @param func Function provide alternate formatting for the x-axis tick labels.
         * @returns {*}
         */
        this.tickFormatX = function (func) {
            this.options.tickFormatX = func;
            return this;
        };
        /**
         * Sets tick hint y
         * @param hint Number the amount of ticks that you would like to have displayed on the y-axis.
         * @returns {*}
         */
        this.tickHintY = function (hint) {
            this.options.tickHintY = hint;
            return this;
        };
        /**
         * Sets tick format y.
         * @param func Function provide alternate formatting for the y-axis tick labels.
         * @returns {*}
         */
        this.tickFormatY = function (func) {
            this.options.tickFormatY = func;
            return this;
        };
        /**
         * Sets data format x.
         * @param func Function a method to pre-format the input data for the x-axis before attempting to compute a
         * scale or draw.
         * @returns {*}
         */
        this.dataFormatX = function (func) {
            this.options.dataFormatX = func;
            return this;
        };
        /**
         * Sets data format y.
         * @param func Function a method to pre-format the input data for the y-axis before attempting to compute a
         * scale or draw.
         * @returns {*}
         */
        this.dataFormatY = function (func) {
            this.options.dataFormatY = func;
            return this;
        };
        /**
         * Sets sort x.
         * @param func Function provide a method to custom-sort x-axis data. Must be a valid array comparator,
         * returning -1, 0, or 1.
         * @returns {*}
         */
        this.sortX = function (func) {
            this.options.sortX = func;
            return this;
        };
        /**
         * Sets unsupported.
         * @param func Function a callback method that will be invoked if SVG is not supported in the viewer's
         * browser.
         * @returns {*}
         */
        this.unsupported = function (func) {
            this.options.unsupported = func;
            return this;
        };
        /**
         * Sets empty.
         * @param func Function a callback method invoked when the data set provided was empty and there is
         * nothing to draw.
         * @returns {*}
         */
        this.empty = function (func) {
            this.options.empty = func;
            return this;
        };
        /**
         * Sets not empty.
         * @param func Function the opposite of empty. Invoked if the data set provided was not empty.
         * @returns {*}
         */
        this.notempty = function (func) {
            this.options.notempty = func;
            return this;
        };
        /**
         * Sets timing.
         * @param timing Number the amount of time, in milliseconds, to transition during draw/update.
         * @returns {*}
         */
        this.timing = function (timing) {
            this.options.timing = timing;
            return this;
        };
        /**
         * Sets interpolation.
         * @param interpolation String line interpolation to use when drawing lines.
         * (see https://github.com/mbostock/d3/wiki/SVG-Shapes#wiki-line_interpolate)
         * @returns {*}
         */
        this.interpolation = function (interpolation) {
            this.options.interpolation = interpolation;
            return this;
        };
    }

    /**
     * Creates an xChart options instance.
     * @returns {XChartOptions}
     */
    var createXChartOptions = function () {
        return new XChartOptions();
    }

    /**
     * XChart data object.
     * @constructor
     */
    function XChartData() {
        this.data = {
            xScale: 'ordinal',
            yScale: 'ordinal',
            main: [
                {
                    className: '.pizza',
                    data: []
                }
            ]

        };
        /**
         * The x scale type
         * @param scale Sting scale type to use along the x-axis (horizontal).
         * Options are ordinal, linear, time, exponential.
         * @returns {*}
         */
        this.xScale = function (scale) {
            this.data.xScale = scale;
            return this;
        }
        /**
         * The y scale type
         * @param scale Sting scale type to use along the y-axis (vertical).
         * Options are ordinal, linear, time, exponential.
         * @returns {*}
         */
        this.yScale = function (scale) {
            this.data.yScale = scale;
            return this;
        }

        /**
         * Set the class name.
         * @param className String a unique CSS Selector of classes to use in identifying the elements on the chart.
         * @returns {*}
         */
        this.className = function (className) {
            this.data.main[0].className = className;
            return this;
        }

        /**
         * Adds a new data entry.
         * @param x The x value.
         * @param y The y value.
         * @returns {*}
         */
        this.addDataEntry = function (x, y) {
            this.data.main[0].data.push({x: x, y: y });
            return this;
        }
    }

    /**
     * Creates an xChart data instance.
     * @returns {XChartData}
     */
    var createXChartData = function () {
        return new XChartData();
    }

    /**
     * JustGage data object.
     * @constructor
     */
    function JustGageData() {
        this.data = {
            identifier: undefined,
            value: undefined,
            title: '',
            levelColors: ['#ff0000', '#ff0000', '#ff0000', '#ff0000', '#ff0000', '#f9c802', '#f9c802', '#f9c802', '#f9c802', '#a9d70b']

        };
        /**
         * The identifier
         * @param identifier String identifier
         * @returns {*}
         */
        this.identifier = function (identifier) {
            this.data.identifier = identifier;
            return this;
        }
        /**
         * The value
         * @param value Number value
         * @returns {*}
         */
        this.value = function (value) {
            this.data.value = value;
            return this;
        }
        /**
         * The title
         * @param title Sting title
         * @returns {*}
         */
        this.title = function (title) {
            this.data.title = title;
            return this;
        }

        /**
         * Set the level colors
         * @param levelColors String[] the levelcolors.
         * @returns {*}
         */
        this.levelColors = function (levelColors) {
            this.data.levelColors = levelColors;
            return this;
        }
    }

    /**
     * Creates an just gage data instance.
     * @returns {JustGageData}
     */
    var createJustGageData = function () {
        return new JustGageData();
    }

    return {
        /**
         * Create a Datatable config instance.
         * @returns {DatatableConfig}
         */
        createDatatableConfig: createDatatableConfig,
        createStateConfig: createStateConfig,
        createXChartOptions: createXChartOptions,
        createXChartData: createXChartData,
        createJustGageData: createJustGageData
    }
});