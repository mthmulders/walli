"use strict";

/**
 * The trusted filter enables you to still use html as trusted.
 */

angular.module('core')
    .filter('trusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);