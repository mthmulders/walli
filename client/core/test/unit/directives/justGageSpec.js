"use strict";

/* jasmine specs for the just-gage directive, */
describe('The justGage directive', function () {
    var scope, compile, factory, timeout, justGage;

    beforeEach(function () {
        module('core');

        inject(function ($rootScope, $compile, $timeout, modelFactory) {
            scope = $rootScope;
            compile = $compile;
            factory = modelFactory;
            timeout = $timeout;
            justGage = {
                refresh: function (value) {
                }
            };
        })
    });

    describe('that is provided with valid data', function () {
        var element;

        beforeEach(function () {
            var gauge = factory.createJustGageData().identifier("identifier").value(90).title("title").data;
            gauge.min = 0;
            gauge.max = 100;
            gauge.widthScale = 0.5;
            gauge.levelColors = ['#ff0000', '#ff0000'];
            gauge.refreshAnimationTime = 2000;
            gauge.refreshAnimationType = 'easeIn';

            scope.gauge = gauge;

            element = angular.element(
                '<div>' +
                '   <just-gage class="justgage" identifier="gauge.identifier" title="gauge.title" width-scale="gauge.widthScale" value="gauge.value" min="gauge.min" max="gauge.max" level-colors="gauge.levelColors" refresh-animation-time="gauge.refreshAnimationTime" refresh-animation-type="gauge.refreshAnimationType"></just-gage>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();
        });

        it("should set the correct style class", function () {
            var div = element.find('div');
            expect(div.length).toBe(1);
            expect(div.eq(0).attr('class')).toContain('justgage');
        });

        it("should set the correct id", function () {
            var div = element.find('div');
            expect(div.eq(0).attr('id')).toEqual('identifier-justgage');
        });

        it("should call the justgage constructor with a valid config", function () {
            spyOn(window, 'JustGage').andReturn(justGage);
            timeout.flush();
            expect(window.JustGage).toHaveBeenCalledWith({
                id: 'identifier-justgage',
                title: 'title',
                gaugeWidthScale: 0.5,
                levelColors: [ '#ff0000', '#ff0000' ],
                value: 90,
                min: 0,
                max: 100,
                refreshAnimationTime: 2000,
                refreshAnimationType: 'easeIn'
            });
        });

        it("should refresh the justgage when the value changes", function () {
            spyOn(window, 'JustGage').andReturn(justGage);
            spyOn(justGage, 'refresh').andCallThrough();
            timeout.flush();
            expect(window.JustGage).toHaveBeenCalledWith({
                id: 'identifier-justgage',
                title: 'title',
                gaugeWidthScale: 0.5,
                levelColors: [ '#ff0000', '#ff0000' ],
                value: 90,
                min: 0,
                max: 100,
                refreshAnimationTime: 2000,
                refreshAnimationType: 'easeIn'
            });

            scope.gauge.value = 20;
            scope.$digest();
            timeout.flush();
            expect(justGage.refresh).toHaveBeenCalledWith(20);
        });
    });

    describe('The view using a without data', function () {
        var element;

        beforeEach(function () {
            var gauge = {};

            element = angular.element(
                '<div>' +
                '   <just-gage class="justgage" identifier="identifier" title="title" width-scale="gauge.widthScale" value="gauge.value" min="gauge.min" max="gauge.max" level-colors="gauge.levelColors"></just-gage>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();

        });

        it("should set the correct style class", function () {
            var div = element.find('div');
            expect(div.length).toBe(1);
            expect(div.eq(0).attr('class')).toContain('justgage');
        });

        it("should set the correct id", function () {
            var div = element.find('div');
            expect(div.eq(0).attr('id')).toEqual('-justgage');
        });

    it("should call the justgage constructor with a default config", function () {
            spyOn(window, 'JustGage').andReturn(justGage);
            timeout.flush();
            expect(window.JustGage).toHaveBeenCalledWith({ id: 'undefined-justgage', title: undefined, gaugeWidthScale: 0.2, levelColors: [  ], value: 0, min: 0, max: 100, refreshAnimationTime: 4000, refreshAnimationType: 'bounce' });
        });
    });
});
