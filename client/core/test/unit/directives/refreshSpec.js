"use strict";

/* jasmine specs for the refresh directive, */
describe('The refresh directive', function () {
    var scope, rootScope, element, timeout;

    beforeEach(function() {
        module('core');

        inject(function ($rootScope, $compile, $timeout) {
            rootScope = $rootScope;
            scope = $rootScope.$new();
            timeout = $timeout;
            spyOn(scope, '$emit').andCallThrough();
            spyOn(timeout, 'cancel').andCallThrough();

            element = angular.element(
                '<div> <div refresh="1"></div> </div>'
            );

            $compile(element)(scope);
            scope.$digest();
        })
    });

    it('should emit a refresh', function () {
        expect(scope.$emit).toHaveBeenCalledWith('refresh');
        expect(scope.$emit.callCount).toBe(1);
    });

    it('should emit a refresh again', function () {
        expect(scope.$emit).toHaveBeenCalledWith('refresh');
        timeout.flush();
        expect(scope.$emit.callCount).toBe(2);
    });

    it('should destroy the refresh', function () {
        expect(scope.$emit).toHaveBeenCalledWith('refresh');
        scope.$broadcast('$destroy');
        expect(timeout.cancel).toHaveBeenCalled();
    });
});
