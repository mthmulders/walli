"use strict";

/* jasmine specs for the state-icon directive, */
describe('The stateIcon directive', function () {
    var scope, compile;

    beforeEach(function() {
        module('core');

        inject(function($rootScope, $compile, modelFactory) {
            scope = $rootScope;
            compile = $compile;

            scope.config = modelFactory.createStateConfig()
                .addState('one', 'green')
                .addState('two', 'red')
                .addState('three', 'orange', true);

            scope.state = 'two';
        })
    });

    describe('that is provided with a state', function() {
        var element;

        beforeEach(function() {
            element = angular.element(
                '<div>' +
                '   <state-icon config="config" state="state"/>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();
        });

        it("should set the correct style class", function() {
            var i = element.find('i');
            expect(i.length).toBe(1);
            expect(i.eq(0).attr('class')).toContain('red');
        });

        it("should show a red state for the second state", function() {
            var i = element.find('i');
            expect(i.eq(0).attr('class')).toContain('red');
        });

        it("should show a green state if the state changes to one", function() {
            var i = element.find('i');
            expect(i.eq(0).attr('class')).toContain('red');

            scope.state = 'one';
            scope.$digest();

            i = element.find('i');
            expect(i.eq(0).attr('class')).toContain('green');
        });
    });

    describe('that is provided without a state', function() {
        var element;

        beforeEach(function() {
            element = angular.element(
                '<div>' +
                '   <state-icon config="config"/>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();
        });

        it("should set the correct style class", function() {
            var i = element.find('i');
            expect(i.length).toBe(1);
            expect(i.eq(0).attr('class')).toContain('orange');
        });

        it("should show an orange state as it is the default", function() {
            var i = element.find('i');
            expect(i.eq(0).attr('class')).toContain('orange');
        });
    });

});
