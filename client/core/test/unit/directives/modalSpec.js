"use strict";

/* jasmine specs for the modal directive, */
describe('The modal directive', function () {
    var scope, rootScope, element;

    beforeEach(function () {
        module('core');

        inject(function ($rootScope, $compile) {
            rootScope = $rootScope;
            scope = $rootScope.$new();
            scope.a = "b";

            element = angular.element(
                '<a name="modal-example" modal="#some-modal" modal-data="a" class="btn btn-default">show/hide modal</a>'
            );
            spyOn(angular, 'element').andReturn(element);

            $compile(element)(scope);
            scope.$digest();
        })
    });

    it('should show/hide modal on click', function () {
        var currentDisplay = 'none';
        var display = undefined;
        spyOn($.fn, 'init').andReturn(
            {
                modal: function (state) {
                    display = state;
                    currentDisplay = state;
                },
                css: function (type) {
                   return currentDisplay;
                }
            }
        );

        expect(display).toBe(undefined);
        element.triggerHandler("click");
        expect(display).toBe('show');
        element.triggerHandler("click");
        expect(display).toBe('hide');
    });
});
