"use strict";

/* jasmine specs for the utils. */
describe('The utils', function () {
    var service

    var firstOfJanuary2013 = 1356994800000;
    var tenthOfOctober2013 = 1381356000000;
    var thirtyFirstOfDecember2014 = 1388466000000;

    beforeEach(function () {
        module('core');
        inject(function (utils) {
            service = utils;
        });
    });

    it('should correctly format the given timestamp as date without year', function () {
        expect(service.date(firstOfJanuary2013)).toEqual('01-01');
        expect(service.date(tenthOfOctober2013)).toEqual('10-10');
        expect(service.date(thirtyFirstOfDecember2014)).toEqual('12-31');
    });
    it('should correctly format the given timestamp as a full date ', function () {
        expect(service.fullDate(firstOfJanuary2013)).toEqual('01-01-2013');
        expect(service.fullDate(tenthOfOctober2013)).toEqual('10-10-2013');
        expect(service.fullDate(thirtyFirstOfDecember2014)).toEqual('12-31-2013');
    });
    it('should correctly remove the time from the given timestamp', function () {
        expect(service.dayTimestamp(firstOfJanuary2013)).toEqual(1356908400000);
    });
    it('should correctly get the duration as readable string', function () {
        expect(service.duration(0.1)).toEqual('6 sec.'); // should not add 0 prefix to seconds
        expect(service.duration(0.2)).toEqual('12 sec.'); // should never add 0 prefix to seconds
        expect(service.duration(1)).toEqual('1 min 00 sec.'); // should add 0 prefix to seconds
        expect(service.duration(1.1)).toEqual('1 min 06 sec.'); // should add 0 prefix to seconds
        expect(service.duration(1.2)).toEqual('1 min 12 sec.'); // should not add 0 prefix to seconds
    });
});
