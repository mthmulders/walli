"use strict";

/* jasmine specs for the notification service. */
describe('The notificationService', function () {
    var service;

    beforeEach(function () {
        module('core');
        inject(function (notificationService) {
            service = notificationService;
        });
    });

    afterEach(function() {
        localStorage.clear();
    })

    it('should add a notification', function () {
        expect(service.getNotifications().length).toBe(0);
        service.addNotification('error', 'title', 'message');
        expect(service.getNotifications().length).toBe(1);
    });
    it('should not add a notification twice', function () {
        expect(service.getNotifications().length).toBe(0);
        service.addNotification('error', 'title', 'message');
        expect(service.getNotifications().length).toBe(1);
        service.addNotification('error', 'title', 'message');
        expect(service.getNotifications().length).toBe(1);
    });
    it('should mark a notification as displayed on removal', function () {
        service.addNotification('error', 'title', 'message');
        var notifications = service.getNotifications();
        var key = notifications[0].key;
        expect(notifications.length).toBe(1);
        expect(notifications[0].displayed).toBe(false);
        service.removeNotification(key);
        notifications = service.getNotifications();
        expect(notifications.length).toBe(1);
        expect(notifications[0].displayed).toBe(true);
    });

    it('should not throw an exception when removing an already removed notification', function () {
        var notifications = service.getNotifications();
        expect(notifications.length).toBe(0);
        service.removeNotification('asdf');
        notifications = service.getNotifications();
        expect(notifications.length).toBe(0);
    });
    it('should get all notifications', function () {
        service.addNotification('error1', 'title1', 'message1');
        service.addNotification('error2', 'title2', 'message2');
        service.addNotification('error3', 'title3', 'message3');
        var notifications = service.getNotifications();
        expect(notifications.length).toBe(3);
        var localStorageNotifications = angular.fromJson(localStorage.getItem('walli-notifications'));
        expect(localStorageNotifications.length).toBe(3);
        expect(localStorageNotifications[0].key).toBe(notifications[0].key);
        expect(localStorageNotifications[0].displayed).toBe(notifications[0].displayed);
        expect(localStorageNotifications[1].key).toBe(notifications[1].key);
        expect(localStorageNotifications[1].displayed).toBe(notifications[1].displayed);
        expect(localStorageNotifications[2].key).toBe(notifications[2].key);
        expect(localStorageNotifications[2].displayed).toBe(notifications[2].displayed);
    });
});
