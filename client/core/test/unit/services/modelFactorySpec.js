"use strict";

/* jasmine specs for the model factory. */
describe('Given the model factory', function () {
    var service

    beforeEach(function () {
        module('core');
        inject(function (modelFactory) {
            service = modelFactory;
        });

    });

    it('should correctly create a datatable config', function () {
        var config = service.createDatatableConfig().
            addColumn('field1', 'description1', 'className', 'template', true).
            addColumn('field2', 'description2', 'className', 'template', false).
            addColumn('field3', 'description3', 'className', 'template', true, 2).
            addRow({field1:'one', field2: 'one', field3: 'one'}).
            addRows([
                {field1:'two', field2: 'two', field3: 'two'},
                {field1:'three', field2: 'three', field3: 'three'}
            ]);

        expect(config.headers.length).toBe(2);
        expect(config.columns.length).toBe(3);
        expect(config.rows.length).toBe(3);
    });

    it('should correctly create a state config', function () {
        var config = service.createStateConfig().
            addState('field1', 'classes1', false).
            addState('field2', 'classes2', true).
            addState('field3', 'classes3', false);

        expect(config.states.length).toBe(3);
        expect(config.default.key).toBe('field2');
    });

    it('should correctly create a xchart options', function () {
        var options = service.createXChartOptions().
            mouseover(function(){return "mouseover";}).
            mouseout(function(){return "mouseout";}).
            click(function(){return "click";}).
            axisPaddingTop(function(){return "axisPaddingTop";}).
            axisPaddingRight(function(){return "axisPaddingRight";}).
            axisPaddingBottom(function(){return "axisPaddingBottom";}).
            axisPaddingLeft(function(){return "axisPaddingLeft";}).
            xMin(function(){return "xMin";}).
            xMax(function(){return "xMax";}).
            yMin(function(){return "yMin";}).
            yMax(function(){return "yMax";}).
            paddingTop(function(){return "paddingTop";}).
            paddingRight(function(){return "paddingRight";}).
            paddingBottom(function(){return "paddingBottom";}).
            paddingLeft(function(){return "paddingLeft";}).
            tickHintX(function(){return "tickHintX";}).
            tickFormatX(function(){return "tickFormatX";}).
            tickHintY(function(){return "tickHintY";}).
            tickFormatY(function(){return "tickFormatY";}).
            dataFormatX(function(){return "dataFormatX";}).
            dataFormatY(function(){return "dataFormatY";}).
            sortX(function(){return "sortX";}).
            unsupported(function(){return "unsupported";}).
            empty(function(){return "empty";}).
            notempty(function(){return "notempty";}).
            timing(function(){return "timing";}).
            interpolation(function(){return "interpolation";}).
        options;

        expect(options.mouseover()).toBe("mouseover");
        expect(options.mouseout()).toBe("mouseout");
        expect(options.click()).toBe("click");
        expect(options.axisPaddingTop()).toBe("axisPaddingTop");
        expect(options.axisPaddingRight()).toBe("axisPaddingRight");
        expect(options.axisPaddingBottom()).toBe("axisPaddingBottom");
        expect(options.axisPaddingLeft()).toBe("axisPaddingLeft");
        expect(options.xMin()).toBe("xMin");
        expect(options.xMax()).toBe("xMax");
        expect(options.yMin()).toBe("yMin");
        expect(options.yMax()).toBe("yMax");
        expect(options.paddingTop()).toBe("paddingTop");
        expect(options.paddingRight()).toBe("paddingRight");
        expect(options.paddingBottom()).toBe("paddingBottom");
        expect(options.paddingLeft()).toBe("paddingLeft");
        expect(options.tickHintX()).toBe("tickHintX");
        expect(options.tickFormatX()).toBe("tickFormatX");
        expect(options.tickHintY()).toBe("tickHintY");
        expect(options.tickFormatY()).toBe("tickFormatY");
        expect(options.dataFormatX()).toBe("dataFormatX");
        expect(options.dataFormatY()).toBe("dataFormatY");
        expect(options.sortX()).toBe("sortX");
        expect(options.unsupported()).toBe("unsupported");
        expect(options.empty()).toBe("empty");
        expect(options.notempty()).toBe("notempty");
        expect(options.timing()).toBe("timing");
        expect(options.interpolation()).toBe("interpolation");
    });

    it('should correctly create a xchart data', function () {
        var data = service.createXChartData().
            xScale("xScale").
            yScale("yScale").
            className("className").
            addDataEntry('one','two').
            addDataEntry('three','four').
            data;

        expect(data.xScale).toBe("xScale");
        expect(data.yScale).toBe("yScale");
        expect(data.main[0].className).toBe("className");
        expect(data.main[0].data[0].x).toBe("one");
        expect(data.main[0].data[0].y).toBe("two");
        expect(data.main[0].data[1].x).toBe("three");
        expect(data.main[0].data[1].y).toBe("four");
    });

    it('should correctly create a justgage data', function () {
        var data = service.createJustGageData().
            identifier("identifier").
            value("value").
            title("title").
            levelColors(['color1','color2','color3']).
            data;

        expect(data.identifier).toBe("identifier");
        expect(data.value).toBe("value");
        expect(data.title).toBe("title");
        expect(data.levelColors.length).toBe(3);
    });
});
