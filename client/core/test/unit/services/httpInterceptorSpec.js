"use strict";

angular.module('mockModule', ['core', 'ngResource']);
angular.module('mockModule').factory('httpService', function ($resource) {
    return $resource('http://localhost/api/proxy/get/me/something');
});
angular.module('mockModule').factory('nonProxiedHttpService', function ($resource) {
    return $resource('/get/me/something');
});
angular.module('mockModule').controller('mockCtrl', function ($scope, httpService, nonProxiedHttpService) {
    $scope.status = 'unknown';

    $scope.triggerHttpCall = function () {
        httpService.get({},
            function success(rsp) {
                $scope.statuscode = rsp.status;
                $scope.status = 'success';
                $scope.response = rsp;
            },
            function error(rsp) {
                $scope.statuscode = rsp.status;
                $scope.status = 'error';
                $scope.response = rsp;
            })
    };
    $scope.triggerNonProxiedHttpCall = function () {
        nonProxiedHttpService.get({},
            function success(rsp) {
                $scope.statuscode = rsp.status;
                $scope.status = 'success';
                $scope.response = rsp;
            },
            function error(rsp) {
                $scope.statuscode = rsp.status;
                $scope.status = 'error';
                $scope.response = rsp;
            })
    };
});


/* jasmine specs for the http interceptor. */
describe('The httpInterceptor', function () {
    var scope, rootScope, ctrl, httpBackend, service, location;
    var result = {iam: 'walli'};

    beforeEach(function () {
        module('mockModule');

        inject(function ($rootScope, $controller, $httpBackend, notificationService, $location) {
            rootScope = $rootScope;
            scope = $rootScope.$new();
            httpBackend = $httpBackend;
            service = notificationService;
            location = $location;
            ctrl = $controller('mockCtrl', {
                $scope: scope, notificationService: service, $location: location
            });

            spyOn(notificationService, 'addNotification').andCallThrough();
            spyOn($location, 'path').andCallThrough();
        });
    });

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("should handle a valid response", function () {
        var headers = {};
        headers['content-type'] = 'application/json';
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(200, result, headers);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.status).toEqual('success');
        expect(scope.response.iam).toBe('walli');
    });
    it("should reject a valid response that doesn't have content-type set to application/json", function () {
        var headers = {};
        headers['content-type'] = 'text/plain';
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(200, result, headers);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(500);
        expect(scope.status).toEqual('error');
    });
    it("should reject a valid response that doesn't have a content-type set", function () {
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(200, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(500);
        expect(scope.status).toEqual('error');
    });
    it("should notify that the backend is offline if status code is 0", function () {
        var result = {iam: 'walli'};
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(0, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(0);
        expect(service.addNotification).toHaveBeenCalledWith('error', 'Backend offline.', '<strong>Walli</strong> was unable to access the server for you. It is currently unavailable');
    });
    it("should notify that the user is not authorized if status code is 401", function () {
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(401, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(401);
        expect(service.addNotification).toHaveBeenCalledWith('error', 'Unauthorized.', 'You do not have the correct authorization.');
    });
    it("should notify that the user is not authenticated if status code is 403", function () {
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(403, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(403);
        expect(service.addNotification).toHaveBeenCalledWith('error', 'Authentication required.', 'You are not authenticated. Please login.');
        expect(location.path).toHaveBeenCalledWith('/login');
    });
    it("should notify that it could not find what your where looking for if status code is between 400 and 500", function () {
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(402, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(402);
        expect(service.addNotification).toHaveBeenCalledWith('error', 'Something went wrong.', 'Walli was unable to find what you are looking for... Sorry!!!');
    });
    it("should notify that the service is unavailable if the status code is 503", function () {
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(503, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(503);
        expect(service.addNotification).toHaveBeenCalledWith('error', 'Service unavailable.', '' +
            '<strong>Walli</strong> was unable to access the service [<span class="status-error" style="font-size: 120%;"><strong>/get/me/something</strong></span>] for you. It is currently unavailable');
    });
    it("should notify that the service is unavailable if the status code is 503 and service is not an api proxied", function () {
        httpBackend.expectGET('/get/me/something').respond(503, result);
        scope.triggerNonProxiedHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(503);
        expect(service.addNotification).toHaveBeenCalledWith('error', 'Service unavailable.', '' +
            '<strong>Walli</strong> was unable to access the service [<span class="status-error" style="font-size: 120%;"><strong>/get/me/something</strong></span>] for you. It is currently unavailable');
    });
    it("should reject if the status code is something else", function () {
        httpBackend.expectGET('http://localhost/api/proxy/get/me/something').respond(118, result);
        scope.triggerHttpCall();
        httpBackend.flush();
        expect(scope.statuscode).toEqual(118);
        expect(service.addNotification).not.toHaveBeenCalled();
    });
});
