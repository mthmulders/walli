package org.walli.di;

import com.google.inject.AbstractModule;
import org.walli.cache.ProxyCache;
import org.walli.cache.SourceCache;

/** Connect Module provides dependencies for connecting with apis. */
public class ConnectModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SourceCache.class);
        bind(ProxyCache.class);
    }
}