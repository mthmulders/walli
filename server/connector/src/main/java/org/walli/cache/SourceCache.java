package org.walli.cache;

import org.walli.domain.Source;
import org.walli.exception.UnknownSourceException;
import org.walli.service.ISourceService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Hashtable;

/**
 * SourceCache is a caching provider that returns the Source for the given name.
 * If a source is not present in the {@link ISourceService} an exception will be thrown.
 */
@Singleton
public final class SourceCache {
    @Inject
    private ISourceService service;

    /** Holds all sources. */
    private Hashtable<String, Source> sources = new Hashtable<String, Source>();

    /**
     * Get the {@link org.walli.domain.Source} with the given source name.
     * @param sourceName The source name.
     * @return source The source.
     */
    public Source getSource(final String sourceName) {
        Source source = sources.get(sourceName);
        if (source == null) {
            source = loadSource(sourceName);
            sources.put(sourceName, source);
        }
        return source;
    }

    /**
     * Update the {@link org.walli.domain.Source} with the given source name.
     * @param sourceName The source name.
     */
    public void updateSource(final String sourceName) {
        final Source source = loadSource(sourceName);
        sources.put(sourceName, source);
    }

    /**
     * Delete the {@link org.walli.domain.Source} with the given source name.
     * @param sourceName The source name.
     */
    public void deleteSource(final String sourceName) {
        sources.remove(sourceName);
    }

    /**
     * Load the {@link org.walli.domain.Source} with the given name from the {@link org.walli.service.SourceService}.
     * @param sourceName The source name.
     * @return source The source.
     */
    private Source loadSource(final String sourceName) {
        final Source source = service.sourceByName(sourceName);
        if (source == null) {
            throw new UnknownSourceException("No Source with name [" + sourceName + "].");
        }
        return source;
    }
}
