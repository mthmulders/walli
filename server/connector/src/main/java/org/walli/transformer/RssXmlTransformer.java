package org.walli.transformer;


import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Transformer that transforms an RSS XML document into a JSON string.
 */
public class RssXmlTransformer {
    private  static final Logger LOG = LoggerFactory.getLogger(RssXmlTransformer.class);

    /**
     * Transform an RSS feed describing a Git commit log into a JSON structure.
     * @param input String representation of the RSS feed.
     * @return String representation of the JSON structure.
     */
    public synchronized List<RssEntry> transform(String input) {
        try {
            SyndFeedInput feedInput = new SyndFeedInput();
            SyndFeed feed = feedInput.build(new StringReader(input));

            List<RssEntry> entries = new ArrayList<RssEntry>(feed.getEntries().size());
            for(SyndEntry entry : (List<SyndEntry>) feed.getEntries()) {
                String title = entry.getTitle();
                Date published = entry.getPublishedDate();

                entries.add(new RssEntry(published, title));
            }

            return entries;

        } catch (FeedException fe) {
            LOG.error("Error parsing input", fe);
            return Collections.emptyList();
        }
    }
}

