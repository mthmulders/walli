package org.walli.connector;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/** X509TrustManaget that accepts all X509 certificates. */
public class TrustAllX509TrustManager implements X509TrustManager {
    /** {@inheritDoc}. */
    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
    }

    /** {@inheritDoc}. */
    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
    }

    /** {@inheritDoc}. */
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
