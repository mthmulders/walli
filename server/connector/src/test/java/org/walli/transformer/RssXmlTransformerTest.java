package org.walli.transformer;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test class for {@link RssXmlTransformer}.
 */
public class RssXmlTransformerTest {
    private RssXmlTransformer transformer = new RssXmlTransformer();

    @Test
    public void shouldConvertFeedWithOneEntry() throws IOException {
        // Setup
        ByteArrayOutputStream inputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedInputStream = new BufferedOutputStream(inputStream);
        IOUtils.copy(
                getClass().getResourceAsStream("TestCase1.rss"),
                bufferedInputStream);
        bufferedInputStream.flush();
        String input = inputStream.toString();

        // Test
        List<RssEntry> entries = transformer.transform(input);

        // Verify
        assertThat(entries.size(), is(1));
        assertThat(entries.get(0).getDate(), is(new Date(1372688510000l)));
        assertThat(entries.get(0).getTitle(), is("Test commit"));
    }

    @Test
    public void shouldReturnEmptyListOnFeedParsingFailure() throws IOException {
        // Setup
        ByteArrayOutputStream inputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedInputStream = new BufferedOutputStream(inputStream);
        IOUtils.copy(
                getClass().getResourceAsStream("TestCase2.rss"),
                bufferedInputStream);
        bufferedInputStream.flush();
        String input = inputStream.toString();

        // Test
        List<RssEntry> entries = transformer.transform(input);

        // Verify
        assertThat(entries.size(), is(0));
    }
}
