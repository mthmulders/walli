package org.walli.connector;

import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.transformer.RssEntry;
import org.walli.transformer.RssXmlTransformer;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static junit.framework.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

/** Test class for {@link Connector} */
@RunWith(PowerMockRunner.class)
@PrepareForTest({
        Connector.class,
        URL.class,
        NetHttpTransport.class,
        HttpRequestFactory.class,
        GenericUrl.class,
        HttpRequest.class,
        HttpResponse.class
})
@PowerMockIgnore("javax.net.ssl.*")
public class ConnectorTest {
    private static final String URL = "URL";
    private static final String INVALID_URL = "invalid url";
    @Mock
    private java.net.URL url;
    @Mock
    private HttpURLConnection urlConnection;
    @Mock
    private OAuthRsaInformation oAuthRsaInformation;
    @Mock
    private OAuthHmacInformation oAuthHmacInformation;
    @Mock
    private BasicAuthInformation basicAuthInformation;
    @Mock
    private NetHttpTransport transport;
    @Mock
    private HttpRequestFactory factory;
    @Mock
    private GenericUrl genericUrl;
    @Mock
    private HttpRequest request;
    @Mock
    private HttpResponse response;
    @Mock
    private SSLContext context;

    @Before
    public void setUp() throws Exception {
        whenNew(NetHttpTransport.class).withNoArguments().thenReturn(transport);
        when(transport.createRequestFactory(isA(OAuthParameters.class))).thenReturn(factory);
        when(factory.buildGetRequest(genericUrl)).thenReturn(request);
        when(request.execute()).thenReturn(response);
        when(response.parseAsString()).thenReturn("response");

        whenNew(java.net.URL.class).withParameterTypes(String.class).withArguments(URL).thenReturn(url);
        whenNew(java.net.URL.class).withParameterTypes(String.class).withArguments(INVALID_URL).thenThrow(MalformedURLException.class);

        when(url.openConnection()).thenReturn(urlConnection);
        when(urlConnection.getInputStream()).thenReturn(IOUtils.toInputStream("result"));

        whenNew(GenericUrl.class).withParameterTypes(String.class).withArguments(URL).thenReturn(genericUrl);
    }

    @Test
    public void shouldSetTrustAllX509TrustManager() throws Exception {
        Constructor<Connector> constructor = Connector.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowException() throws Exception {
        mockStatic(SSLContext.class);
        when(SSLContext.getInstance("SSL")).thenThrow(Exception.class);
        Constructor<Connector> constructor = Connector.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void shouldGetResponse200WhenGettingResponseForUnprotectedUrl() throws Exception {
        whenNew(java.net.URL.class).withParameterTypes(String.class).withArguments(URL).thenReturn(url);
        when(url.openConnection()).thenReturn(urlConnection);
        when(urlConnection.getInputStream()).thenReturn(IOUtils.toInputStream("result"));
        assertEquals(200, Connector.getResponse(URL, new AnonymousAuthInformation()).getStatus());
        verify(urlConnection, times(1)).getInputStream();
    }

    @Test
    public void shouldGetResponse503WhenUrlIsNotValidWhenGettingResponseForUnprotectedUrl() throws Exception {
        assertEquals(503, Connector.getResponse(INVALID_URL, new AnonymousAuthInformation()).getStatus());
    }

    @Test
    public void shouldGetResponse200WhenGettingResponseForProtectedOAuthRSAUrl() throws Exception {
        assertEquals(200, Connector.getResponse(URL, oAuthRsaInformation).getStatus());
    }

    @Test
    public void shouldGetResponse500WhenUrlIsNotValidWhenGettingResponseForProtectedOauthRSAUrl() throws Exception {
        whenNew(GenericUrl.class).withParameterTypes(String.class).withArguments(URL).thenThrow(IllegalArgumentException.class);
        assertEquals(500, Connector.getResponse(URL, oAuthRsaInformation).getStatus());
    }

    @Test
    public void shouldGetResponse503WhenIOExceptionOccursWhenGettingResponseForProtectedOauthRSAUrl() throws Exception {
        when(response.parseAsString()).thenThrow(IOException.class);
        assertEquals(503, Connector.getResponse(URL, oAuthRsaInformation).getStatus());
    }

    @Test
    public void shouldGetResponse200WhenGettingResponseForProtectedOAuthHmacUrl() throws Exception {
        assertEquals(200, Connector.getResponse(URL, oAuthHmacInformation).getStatus());
    }

    @Test
    public void shouldGetResponse500WhenUrlIsNotValidWhenGettingResponseForProtectedOauthHmacUrl() throws Exception {
        whenNew(GenericUrl.class).withParameterTypes(String.class).withArguments(URL).thenThrow(IllegalArgumentException.class);
        assertEquals(500, Connector.getResponse(URL, oAuthHmacInformation).getStatus());
    }

    @Test
    public void shouldGetResponse503WhenIOExceptionOccursWhenGettingResponseForProtectedOauthHmacUrl() throws Exception {
        when(response.parseAsString()).thenThrow(IOException.class);
        assertEquals(503, Connector.getResponse(URL, oAuthHmacInformation).getStatus());
    }

    @Test
    public void shouldGetResponse200WhenGettingResponseForProtectedBasicAuthUrl() throws Exception {
        when(basicAuthInformation.getUserPass()).thenReturn("username:password");
        assertEquals(200, Connector.getResponse(URL, basicAuthInformation).getStatus());
    }

    @Test
    public void shouldGetResponse503WhenUrlIsNotValidWhenGettingResponseForUnprotectedBasicAuthUrl() throws Exception {
        assertEquals(503, Connector.getResponse(INVALID_URL, basicAuthInformation).getStatus());
    }

    @Test
    public void shouldNotBeAbleToCallPrivateConstructor() throws Exception {
        final Constructor<?>[] constructors = Connector.class.getDeclaredConstructors();
        assertEquals(1, constructors.length);
        final Constructor<?> defaultConstructor = constructors[0];
        assertFalse(defaultConstructor.isAccessible());
        assertNotNull(defaultConstructor.newInstance());
    }

    @Test
    public void shouldCallRssXmlTransformerToConvertRssResponse() throws Exception {
        // Setup
        whenNew(java.net.URL.class).withParameterTypes(String.class).withArguments(URL).thenReturn(url);
        when(url.openConnection()).thenReturn(urlConnection);
        when(urlConnection.getInputStream()).thenReturn(RssXmlTransformer.class.getResourceAsStream("TestCase1.rss"));
        when(urlConnection.getContentType()).thenReturn("application/rss+xml");

        // Test
        Response response = Connector.getResponse(URL, new AnonymousAuthInformation());
        assertThat(response.getStatus(), is(200));
        assertThat(((List<RssEntry>) response.getEntity()).size(), is(1));
    }
}
