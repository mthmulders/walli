package org.walli.di;

import com.google.inject.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.walli.cache.SourceCache;
import org.walli.service.ISourceService;

import javax.inject.Inject;

/** Test class for {@link ConnectModule}. */
@RunWith(MockitoJUnitRunner.class)
public class ConnectModuleTest {
    private Module module;
    @Mock
    private ISourceService service;
    @Inject
    private SourceCache provider;

    @Before
    public void setUp() {
        module = new ConnectModule();
    }

    @Test(expected = CreationException.class)
    public void shouldThrowExceptionWhenOnlyCommonModuleIsLoadedSourceProvider() throws Exception {
        final Injector injector = Guice.createInjector(module);
        injector.injectMembers(this);
    }

    @Test
    public void shouldBindSourceProvider() throws Exception {
        final Injector injector = Guice.createInjector(module, new AbstractModule() {
            @Override
            protected void configure() {
                bind(ISourceService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);
    }
}
