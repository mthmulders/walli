package org.walli.cache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.walli.connector.AuthenticationInformation;

import static org.junit.Assert.assertEquals;

/** Test class for {@link ProxyRequest}. */
@RunWith(MockitoJUnitRunner.class)
public class ProxyRequestTest {
    @Mock
    private AuthenticationInformation information;

    @Test
    public void shouldSetValues() throws Exception {
        final ProxyRequest proxyRequest = new ProxyRequest("url", information);
        assertEquals("url", proxyRequest.getUrl());
        assertEquals(information, proxyRequest.getInformation());
    }
}
