package org.walli.exception;

import com.google.inject.Singleton;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Persistence exceptionMapper takes care of returning the persistence validations. */
@Singleton
@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {

    @Override
    public Response toResponse(final PersistenceException e) {
        final Response.Status status;

        final Pattern pattern = Pattern.compile("^(Unique).*\\((.*)\\).*\\]$");
        final Matcher matcher = pattern.matcher(e.getCause().getCause().getMessage());

        Violation violation;
        if (matcher.matches()) {
            violation = new Violation(matcher.group(2).toLowerCase(), "not unique");
        } else {
            violation = new Violation(null, e.getMessage());
        }
        final GenericEntity entity = new GenericEntity<Violation>(violation) {
        };
        status = Response.Status.BAD_REQUEST;
        return Response.status(status).entity(entity).build();
    }

}