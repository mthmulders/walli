package org.walli.api;

/** Media Types used by Walli. */
public final class WalliMediaTypes {
    public static final String CONFIGURATION_VND = "application/vnd.org.walli.domain.configuration+json";
    public static final String PROJECT_VND = "application/vnd.org.walli.domain.project+json";
    public static final String SOURCE_VND = "application/vnd.org.walli.domain.source+json";
    public static final String USER_VND = "application/vnd.org.walli.domain.user+json";
}
