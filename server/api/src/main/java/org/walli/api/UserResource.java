package org.walli.api;

import com.google.inject.persist.Transactional;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.walli.domain.User;
import org.walli.service.IUserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.walli.api.WalliMediaTypes.USER_VND;

/** Resource that exposes the user api. */
@Path("/users")
public class UserResource {
    private static final Logger LOG = LoggerFactory.getLogger(UserResource.class);
    @Inject
    private IUserService service;

    /**
     * Gets the users.
     * @return user The users.
     */
    @GET
    @Produces(USER_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    public Response users() {
        LOG.debug("Getting all users.");
        List<User> users = service.users();
        return Response.ok(new GenericEntity<List<User>>(users) {
        }).build();
    }

    @Path("/{id}")
    @GET
    @Produces(USER_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    public Response user(@PathParam("id") final Long id) {
        LOG.debug("Getting user with id" + id + ".");
        final User user = service.userById(id);
        return Response.ok(new GenericEntity<User>(user) {
        }).build();
    }

    @POST
    @Consumes({USER_VND, MediaType.APPLICATION_JSON})
    @Produces(USER_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response create(final User user) {
        LOG.debug("create user");
        service.add(user);
        return Response.ok(new GenericEntity<User>(user) {
        }).build();
    }

    @Path("/{id}")
    @PUT
    @Consumes({USER_VND, MediaType.APPLICATION_JSON})
    @Produces(USER_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response update(@PathParam("id") final Long id, final User user) {
        LOG.debug("Updating user with id" + id + ".");
        service.update(user);
        return Response.ok(new GenericEntity<User>(user) {
        }).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(USER_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response delete(@PathParam("id") final Long id) {
        LOG.debug("Delete user with id" + id + ".");
        boolean hasAdminLeft = false;
        for(final User user : service.admins()) {
            if(user.getId() != id) {
                hasAdminLeft = true;
                break;
            }
        }
        final Response response;
        if(hasAdminLeft) {
            service.delete(id);
            response = Response.ok().build();
        } else {
            response = Response.status(409).entity("Cannot delete the given user, no available admin left.").build();
        }
        return response;
    }
}
