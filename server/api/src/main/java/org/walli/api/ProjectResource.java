package org.walli.api;

import com.google.inject.persist.Transactional;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.walli.domain.Project;
import org.walli.service.IProjectService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.walli.api.WalliMediaTypes.PROJECT_VND;

/** Resource that exposes the project api. */
@Path("/projects")
public class ProjectResource {
    private static final Logger LOG = LoggerFactory.getLogger(ProjectResource.class);
    @Inject
    private IProjectService service;

    /**
     * Gets the projects.
     * @return project The projects.
     */
    @GET
    @Produces(PROJECT_VND)
    public Response projects() {
        LOG.debug("Getting all projects.");
        final List<Project> projects = service.projects();
        return Response.ok(new GenericEntity<List<Project>>(projects) {
        }).build();
    }

    /**
     * Gets the project matching the given id.
     * @param id The id.
     * @return project The project.
     */
    @Path("/{id}")
    @GET
    @Produces(PROJECT_VND)
    public Response project(@PathParam("id") final Long id) {
        LOG.debug("Getting project with id" + id + ".");
        final Project project = service.projectById(id, true);
        return Response.ok(new GenericEntity<Project>(project) {
        }).build();
    }

    /**
     * Creates a project.
     * @param project The project.
     * @return project The project.
     */
    @POST
    @Consumes({PROJECT_VND, MediaType.APPLICATION_JSON})
    @Produces(PROJECT_VND)
    @RequiresAuthentication
    @Transactional
    public Response create(final Project project) {
        LOG.debug("create project");
        service.add(project);
        return Response.ok(new GenericEntity<Project>(project) {
        }).build();
    }

    /**
     * Updates the given project.
     * @param id      The id.
     * @param project The project.
     * @return project The project.
     */
    @Path("/{id}")
    @PUT
    @Consumes({PROJECT_VND, MediaType.APPLICATION_JSON})
    @Produces(PROJECT_VND)
    @RequiresAuthentication
    @Transactional
    public Response update(@PathParam("id") final Long id, final Project project) {
        LOG.debug("Updating project with id" + id + ".");
        service.update(project);
        return Response.ok(new GenericEntity<Project>(project) {
        }).build();
    }

    /**
     * Deletes the project matching the given id..
     * @param id The id.
     * @return project The project.
     */
    @Path("/{id}")
    @DELETE
    @Produces(PROJECT_VND)
    @RequiresAuthentication
    @Transactional
    public Response delete(@PathParam("id") final Long id) {
        LOG.debug("Delete project with id" + id + ".");
        service.delete(id);
        return Response.ok().build();
    }
}
