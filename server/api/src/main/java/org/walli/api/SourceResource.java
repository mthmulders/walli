package org.walli.api;

import com.google.inject.persist.Transactional;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.walli.cache.SourceCache;
import org.walli.domain.Source;
import org.walli.service.ISourceService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.walli.api.WalliMediaTypes.SOURCE_VND;

/** Resource that exposes the source api. */
@Path("/sources")
public class SourceResource {
    private static final Logger LOG = LoggerFactory.getLogger(SourceResource.class);
    @Inject
    private ISourceService service;
    @Inject
    private SourceCache cache;

    /**
     * Gets the sources.
     * @return source The sources.
     */
    @GET
    @Produces(SOURCE_VND)
    public Response sources() {
        LOG.debug("Getting all sources.");
        final List<Source> sources = service.sources();
        for (final Source source : sources) {
            source.setOauthToken(null);
            source.setOauthSecret(null);
            source.setPassword(null);
            source.setConsumerSecret(null);
        }

        return Response.ok(new GenericEntity<List<Source>>(sources) {
        }).build();
    }

    @Path("/{id}")
    @GET
    @Produces(SOURCE_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    public Response source(@PathParam("id") final Long id) {
        LOG.debug("Getting source with id" + id + ".");
        final Source source = service.sourceById(id);
        source.setOauthToken(null);
        source.setOauthSecret(null);
        source.setPassword(null);
        source.setConsumerSecret(null);
        return Response.ok(new GenericEntity<Source>(source) {
        }).build();
    }

    @POST
    @Consumes({SOURCE_VND, MediaType.APPLICATION_JSON})
    @Produces(SOURCE_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response create(final Source source) {
        LOG.debug("create source");
        service.add(source);
        return Response.ok(new GenericEntity<Source>(source) {
        }).build();
    }

    @Path("/{id}")
    @PUT
    @Consumes({SOURCE_VND, MediaType.APPLICATION_JSON})
    @Produces(SOURCE_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response update(@PathParam("id") final Long id, final Source source) {
        LOG.debug("Updating source with id" + id + ".");
        service.update(source);
        cache.updateSource(source.getName());
        return Response.ok(new GenericEntity<Source>(source) {
        }).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(SOURCE_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response delete(@PathParam("id") final Long id) {
        LOG.debug("Delete source with id" + id + ".");
        final String sourceName = service.sourceById(id).getName();
        service.delete(id);
        cache.deleteSource(sourceName);
        return Response.ok().build();
    }
}
