package org.walli.api;

import org.walli.cache.ProxyCache;
import org.walli.cache.ProxyRequest;
import org.walli.cache.ProxyResponse;
import org.walli.connector.AnonymousAuthInformation;
import org.walli.connector.BasicAuthInformation;
import org.walli.connector.OAuthHmacInformation;
import org.walli.connector.OAuthRsaInformation;
import org.walli.cache.SourceCache;
import org.walli.domain.Authentication;
import org.walli.domain.Source;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;

import static org.walli.connector.Connector.getResponse;

/** Resource that proxies the source api's. */
@Path("/proxy")
public class ProxyResource {
    @Inject
    private SourceCache provider;
    @Inject
    private ProxyCache cache;

    /**
     * Clear the cache.
     * @return response The response.
     */
    @Path("/clear-cache")
    @GET
    public Response clear() {
        cache.clear();
        return Response.ok().build();
    }

    /**
     * Proxies the given service with the given query.
     * @return response The response.
     */
    @Path("/{service}/{query}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response query(@PathParam("service") final String service, @PathParam("query") final String query, @HeaderParam("refresh") boolean refresh) throws GeneralSecurityException {
        final Source source = provider.getSource(service);
        if (source == null) {
            return Response.status(400).entity("No service with name [" + service + "] found").build();
        }

        final StringBuilder builder = new StringBuilder();
        builder.append(source.getUrl());
        builder.append("/");
        builder.append(query);

        final Response response;
        if (source.getAuthentication() == Authentication.rsa) {
            response = getLiveOrCachedResponse(new ProxyRequest<OAuthRsaInformation>(builder.toString(), new OAuthRsaInformation(source.getConsumerKey(), source.getPrivateKey(), source.getOauthToken())), refresh);
        } else if (source.getAuthentication() == Authentication.hmac) {
            response = getLiveOrCachedResponse(new ProxyRequest<OAuthHmacInformation>(builder.toString(), new OAuthHmacInformation(source.getConsumerKey(), source.getConsumerSecret(), source.getOauthToken(), source.getOauthSecret())), refresh);
        } else if (source.getAuthentication() == Authentication.basic) {
            response = getLiveOrCachedResponse(new ProxyRequest<BasicAuthInformation>(builder.toString(), new BasicAuthInformation(source.getUsername(), source.getPassword())), refresh);
        } else {
            response = getLiveOrCachedResponse(new ProxyRequest<AnonymousAuthInformation>(builder.toString(), new AnonymousAuthInformation()), refresh);
        }
        return response;
    }

    /**
     * Get the response from the cache, if it does not contain any cached response or it has expired, fetch a live response.
     * @param request The request.
     * @return response The response.
     */
    private Response getLiveOrCachedResponse(final ProxyRequest request, final boolean refresh) {
        final Response response;
        final ProxyResponse cachedResponse = cache.getCachedResponse(request);
        if (cachedResponse != null && !refresh) {
            response = cachedResponse.getResponse();
        } else {
            final Response liveResponse = getResponse(request.getUrl(), request.getInformation());
            cache.add(request, new ProxyResponse(System.currentTimeMillis() + 50000, liveResponse));
            response = liveResponse;
        }
        return response;
    }

    /**
     * Checks if the given service is up and running.
     * @return response The response.
     */
    @Path("/{service}/keepalive")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response keepalive(@PathParam("service") final String service) {
        final Source source = provider.getSource(service);
        if (source == null) {
            return Response.status(400).entity("No service with name [" + service + "] found").build();
        }

        Response response;
        try {
            response = getResponse(getBaseUrl(source.getUrl()), new AnonymousAuthInformation());
            response.getMetadata().putSingle("content-type", "application/json");
        } catch (MalformedURLException e) {
            response = Response.serverError().build();
        }
        return response;
    }


    /**
     * Gets the base url.
     * @param url The complete url
     * @return baseUrl The base url.
     * @throws java.net.MalformedURLException If the url is malformed.
     */
    public String getBaseUrl(final String url) throws MalformedURLException {
        final URL u = new URL(url);
        final String host = u.getHost();
        final String protocol = u.getProtocol();
        final Integer port = u.getPort();

        return new URL(protocol, host, port, "").toString();
    }
}
