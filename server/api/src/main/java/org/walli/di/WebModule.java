package org.walli.di;

import com.google.inject.Provides;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistFilter;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.aopalliance.intercept.MethodInterceptor;
import org.apache.shiro.authz.annotation.*;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.walli.api.*;
import org.walli.exception.AuthorizationExceptionMapper;
import org.walli.exception.ConstraintViolationExceptionMapper;
import org.walli.exception.PersistenceExceptionMapper;
import org.walli.security.WalliShiroFilter;
import org.walli.security.WalliShiroRealm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import static com.google.inject.matcher.Matchers.annotatedWith;
import static com.google.inject.matcher.Matchers.any;

/** Web Module. */
public class WebModule extends ServletModule {
    protected void configureServlets() {
        filter("/api/*").through(PersistFilter.class);

        bind(ProjectResource.class);
        bind(SourceResource.class);
        bind(UserResource.class);
        bind(ProxyResource.class);
        bind(AuthenticationResource.class);
        bind(ConfigurationResource.class);
        bind(AuthorizationExceptionMapper.class);
        bind(ConstraintViolationExceptionMapper.class);
        bind(PersistenceExceptionMapper.class);

        final Properties properties = new Properties();
        try {
            properties.load(WebModule.class.getResourceAsStream("/walli.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not load properties.", e);
        }
        bind(Properties.class).annotatedWith(Names.named("walli")).toInstance(properties);

        final HashMap<String, String> guiceMap = new HashMap<String, String>();
        guiceMap.put("com.sun.jersey.config.property.packages", "org.walli.api;org.codehaus.jackson.jaxrs");
        guiceMap.put(PackagesResourceConfig.PROPERTY_RESOURCE_FILTER_FACTORIES, "com.sun.jersey.api.container.filter.RolesAllowedResourceFilterFactory");
        serve("/api/*").with(GuiceContainer.class, guiceMap);
//        filter("/api/*").through(CORSFilter.class);

        MethodInterceptor interceptor = new AopAllianceAnnotationsAuthorizingMethodInterceptor();
        bindInterceptor(any(), annotatedWith(RequiresRoles.class), interceptor);
        bindInterceptor(any(), annotatedWith(RequiresPermissions.class), interceptor);
        bindInterceptor(any(), annotatedWith(RequiresAuthentication.class), interceptor);
        bindInterceptor(any(), annotatedWith(RequiresUser.class), interceptor);
        bindInterceptor(any(), annotatedWith(RequiresGuest.class), interceptor);

        bind(Realm.class).to(WalliShiroRealm.class);

        filter("/*").through(WalliShiroFilter.class);
    }

    @Provides
    public WebSecurityManager provideWebSecurityManager(Realm realm) {
        return new DefaultWebSecurityManager(realm);
    }
}