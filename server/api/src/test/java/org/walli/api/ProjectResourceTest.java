package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.domain.Project;
import org.walli.service.IProjectService;
import org.walli.service.ProjectService;

import javax.inject.Inject;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/** Test class for {@link ProjectResource}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ProjectResource.class, ProjectService.class})
public class ProjectResourceTest {
    @Inject
    private ProjectResource resource;
    @Mock
    private IProjectService service;
    @Mock
    private Project project;

    @Before
    public void setUp() throws Exception {

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ProjectResource.class); // As it is a Jax-rs resource it will be picked up.
                bind(IProjectService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);

    }

    @Test
    public void shouldGetProjects() throws Exception {
        resource.projects();

        verify(service, times(1)).projects();
    }

    @Test
    public void shouldGetProject() throws Exception {
        when(service.projectById(isA(Long.class), isA(Boolean.class))).thenReturn(project);
        final Response response = resource.project(1L);
        assertEquals(project, ((GenericEntity) response.getEntity()).getEntity());
    }

    @Test
    public void shouldAddProject() throws Exception {
        resource.create(project);

        verify(service, times(1)).add(eq(project));
    }

    @Test
    public void shouldUpdateProject() throws Exception {
        resource.update(1L, project);

        verify(service, times(1)).update(eq(project));
    }

    @Test
    public void shouldDeleteProject() throws Exception {
        resource.delete(1L);

        verify(service, times(1)).delete(eq(1L));
    }
}
