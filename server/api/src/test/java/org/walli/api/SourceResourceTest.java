package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.cache.SourceCache;
import org.walli.domain.Source;
import org.walli.service.ISourceService;
import org.walli.service.SourceService;

import javax.inject.Inject;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/** Test class for {@link SourceResource}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({SourceResource.class, SourceService.class, SourceCache.class})
public class SourceResourceTest {
    @Inject
    private SourceResource resource;
    @Mock
    private ISourceService service;
    @Mock
    private SourceCache provider;
    @Mock
    private Source source;

    @Before
    public void setUp() throws Exception {

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(SourceResource.class); // As it is a Jax-rs resource it will be picked up.
                bind(ISourceService.class).toInstance(service);
                bind(SourceCache.class).toInstance(provider);
            }
        });
        injector.injectMembers(this);
    }

    @Test
    public void shouldGetSources() throws Exception {
        when(service.sources()).thenReturn(Arrays.asList(new Source[]{source}));
        resource.sources();

        verify(service, times(1)).sources();
    }

    @Test
    public void shouldGetSource() throws Exception {
        when(service.sourceById(isA(Long.class))).thenReturn(source);
        final Response response = resource.source(1L);
        assertEquals(source, ((GenericEntity) response.getEntity()).getEntity());
    }

    @Test
    public void shouldAddSource() throws Exception {
        resource.create(source);

        verify(service, times(1)).add(eq(source));
    }

    @Test
    public void shouldUpdateSource() throws Exception {
        resource.update(1L, source);

        verify(service, times(1)).update(eq(source));
    }

    @Test
    public void shouldDeleteSource() throws Exception {
        when(service.sourceById(isA(Long.class))).thenReturn(source);
        resource.delete(1L);

        verify(service, times(1)).delete(eq(1L));
    }
}
