package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.walli.domain.Configuration;
import org.walli.service.IConfigurationService;

import javax.inject.Inject;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.when;

/** Test class for {@link ConfigurationResource}. */
@RunWith(MockitoJUnitRunner.class)
public class ConfigurationResourceTest {
    @Inject
    private ConfigurationResource resource;
    @Mock
    private IConfigurationService service;
    @Mock
    private Configuration configuration;

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ConfigurationResource.class); // As it is a Jax-rs resource it will be picked up.
                bind(IConfigurationService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);
    }

    @Test
    public void shouldGetConfigurations() {
        resource.configuration();

        verify(service, times(1)).configurations();
    }

    @Test
    public void shouldGetConfiguration() throws Exception {
        when(service.configurationById(isA(Long.class))).thenReturn(configuration);
        final Response response = resource.configuration(1L);
        assertEquals(configuration, ((GenericEntity) response.getEntity()).getEntity());
    }

    @Test
    public void shouldAddConfiguration() throws Exception {
        resource.create(configuration);

        verify(service, times(1)).add(eq(configuration));
    }

    @Test
    public void shouldUpdateConfiguration() throws Exception {
        resource.update(1L, configuration);

        verify(service, times(1)).update(eq(configuration));
    }

    @Test
    public void shouldNotDeleteConfiguration() throws Exception {
        PowerMockito.when(service.configurationById(isA(Long.class))).thenReturn(configuration);
        when(configuration.isSystem()).thenReturn(true);
        resource.delete(1L);

        verify(service, never()).delete(eq(1L));
    }

    @Test
    public void shouldDeleteConfiguration() throws Exception {
        when(service.configurationById(isA(Long.class))).thenReturn(configuration);
        when(configuration.isSystem()).thenReturn(false);
        resource.delete(1L);

        verify(service, times(1)).delete(eq(1L));
    }
}
