package org.walli.service;

import org.walli.domain.Project;

import javax.persistence.EntityManager;
import java.util.List;

/** Service interface for projects. */
public interface IProjectService {
    /**
     * Gets all projects.
     * @return projects The projects.
     */
    List<Project> projects();

    /**
     * Gets the project.
     * @param id The id.
     * @return project The project
     */
    Project projectById(final Long id, boolean addMissingSourceInformation);

    /**
     * Adds missing source information to the given project.
     * @param project The project.
     * @return project The project.
     */
    Project addMissingSourceInformation(final Project project, final EntityManager em);

    /**
     * Add a project.
     * @param project The project.
     */
    void add(final Project project);

    /**
     * Update a project.
     * @param project The project.
     */
    void update(final Project project);


    /**
     * Delete the project matching the given id.
     * @param id The id.
     */
    void delete(final Long id);

}
