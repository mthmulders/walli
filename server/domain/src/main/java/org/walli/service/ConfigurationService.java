package org.walli.service;

import com.google.inject.Provider;
import org.walli.domain.Configuration;
import org.walli.domain.Project;
import org.walli.domain.Source;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/** Configuration service implementation. */
public class ConfigurationService implements IConfigurationService {
    @Inject
    private Provider<EntityManager> provider;

    /** {@inheritDoc}. */
    public List<Configuration> configurations() {
        return (List<Configuration>) provider.get().createNamedQuery("Configuration.findAll").getResultList();
    }

    /** {@inheritDoc}. */
    public Configuration configurationById(final Long id) {
        return (Configuration) provider.get().createNamedQuery("Configuration.byId").setParameter("id", id).getSingleResult();
    }

    /** {@inheritDoc}. */
    public void add(final Configuration configuration) {
        provider.get().persist(configuration);
    }

    /** {@inheritDoc}. */
    public void update(final Configuration configuration) {
        final EntityManager em = provider.get();
        final Configuration found = configurationById(configuration.getId());
        found.setValue(configuration.getValue());
        em.persist(found); // updated configuration.
    }

    /** {@inheritDoc}. */
    public void delete(final Long id) {
        final EntityManager em = provider.get();
        em.remove(configurationById(id));
    }
}
