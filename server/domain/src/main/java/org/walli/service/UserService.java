package org.walli.service;

import com.google.inject.Provider;
import org.walli.domain.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

/** User service implementation. */
@Singleton
public class UserService implements IUserService {
    @Inject
    private Provider<EntityManager> provider;

    /** {@inheritDoc}. */
    public List<User> users() {
        return (List<User>) provider.get().createNamedQuery("User.findAll").getResultList();
    }

    /** {@inheritDoc}. */
    public User userById(final Long id) {
        return (User) provider.get().createNamedQuery("User.byId").setParameter("id", id).getSingleResult();
    }

    /** {@inheritDoc}. */
    public User userByUsername(final String username) {
        return (User) provider.get().createNamedQuery("User.byUsername").setParameter("username", username).getSingleResult();
    }

    /** {@inheritDoc}. */
    public List<User> admins() {
        return (List<User>) provider.get().createNamedQuery("User.admins").getResultList();
    }

    /** {@inheritDoc}. */
    public void add(final User user) {
        provider.get().persist(user);
    }

    /** {@inheritDoc}. */
    public void update(final User user) {
        final EntityManager em = provider.get();
        final User found = userById(user.getId());
        found.setFirstName(user.getFirstName());
        found.setLastName(user.getLastName());
        found.setEmail(user.getEmail());
        if (!user.getPassword().equals("unchanged")) {
            found.setPassword(user.getPassword());
        }
        found.setAdmin(user.isAdmin());
        em.persist(found);// updated User.
    }

    /** {@inheritDoc}. */
    public void delete(final Long id) {
        final EntityManager em = provider.get();
        em.remove(userById(id));
    }
}
