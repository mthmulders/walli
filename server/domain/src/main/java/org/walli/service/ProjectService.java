package org.walli.service;

import com.google.inject.Provider;
import org.walli.domain.Project;
import org.walli.domain.Source;
import org.walli.domain.SourceInformation;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Project service implementation. */
@Singleton
public class ProjectService implements IProjectService {
    @Inject
    private Provider<EntityManager> provider;

    /** {@inheritDoc}. */
    public List<Project> projects() {
        return (List<Project>) provider.get().createNamedQuery("Project.findAll").getResultList();
    }

    /** {@inheritDoc}. */
    public Project projectById(final Long id, boolean addMissingSourceInformation) {
        final EntityManager em = provider.get();
        final Project project = (Project) em.createNamedQuery("Project.byId").setParameter("id", id).getSingleResult();
        if (addMissingSourceInformation) {
            addMissingSourceInformation(project, em);
        }
        return project;
    }

    /** {@inheritDoc}. */
    public Project addMissingSourceInformation(final Project project, final EntityManager em) {
        final Set<SourceInformation> infos = new HashSet<SourceInformation>();

        final List<Source> sources = (List<Source>) em.createNamedQuery("Source.findAll").getResultList();
        final Set<SourceInformation> informations = project.getInfos();

        for (final Source source : sources) {
            final SourceInformation sourceInformation = new SourceInformation();

            sourceInformation.setProject(project);
            sourceInformation.setName(source.getName());
            sourceInformation.setValue("");
            for (SourceInformation info : informations) {
                if (info.getName().equals(source.getName())) {
                    sourceInformation.setValue(info.getValue());
                    break;
                }
            }
            infos.add(sourceInformation);
        }

        informations.clear();
        informations.addAll(infos);
        return project;
    }

    /** {@inheritDoc}. */
    public void add(final Project project) {
        final EntityManager em = provider.get();
        for (final SourceInformation i : project.getInfos()) {
            i.setProject(project);
        }

        em.persist(project);
    }

    /** {@inheritDoc}. */
    public void update(final Project project) {
        final EntityManager em = provider.get();
        final Project found = projectById(project.getId(), false);
        found.setName(project.getName());
        found.setDisplayName(project.getDisplayName());
        final Set<SourceInformation> informations = found.getInfos();
        for (final SourceInformation i : project.getInfos()) {
            final SourceInformation currentSourceInformation = getCurrentSourceInformation(informations, i.getName());
            if (currentSourceInformation != null) {
                currentSourceInformation.setValue(i.getValue());
                currentSourceInformation.setProject(found);
            } else {
                informations.add(i);
                i.setProject(found);
            }
        }

        em.persist(found);// updated project.
    }

    /** {@inheritDoc}. */
    public void delete(final Long id) {
        final EntityManager em = provider.get();
        em.remove(projectById(id, true));
    }

    /**
     * Get the SourceInformation matching the given name.
     * @param informations The {@link java.util.Set} of {@link org.walli.domain.SourceInformation}.
     * @param name         The name.
     * @return info The {@link org.walli.domain.SourceInformation}.
     */
    private SourceInformation getCurrentSourceInformation(final Set<SourceInformation> informations, final String name) {
        for (SourceInformation info : informations) {
            if (info.getName().equals(name)) {
                return info;
            }
        }
        return null;
    }
}
