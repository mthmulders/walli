package org.walli.di;

import com.google.inject.AbstractModule;
import org.walli.service.*;

/** Guice Service module. */
public class ServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(IProjectService.class).to(ProjectService.class);
        bind(ISourceService.class).to(SourceService.class);
        bind(IUserService.class).to(UserService.class);
        bind(IConfigurationService.class).to(ConfigurationService.class);
    }
}
