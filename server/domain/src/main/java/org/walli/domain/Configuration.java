package org.walli.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Configuration domain class which represents a configuration settings.
 */
@Entity
@Table(name = "Configuration", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@NamedQueries({
        @NamedQuery(name = "Configuration.findAll", query = "SELECT c FROM Configuration c"),
        @NamedQuery(name = "Configuration.byId", query = "SELECT c FROM Configuration c where c.id = :id ")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Configuration {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(min = 1, max = 25)
    @Pattern(regexp = "[A-Za-z1-9_-]*", message = "This is not a valid name, it must contain only letters dashes and underscores")
    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    @Column(name = "system")
    private boolean system = false;

    /** Constructor. */
    public Configuration() {
    }

    /**
     * Gets the id.
     * @return id The id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the name.
     * @return name The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param name The name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the value.
     * @return value The value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     * @param value The value.
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Indicator is system setting.
     * @return system Indicator is system setting.
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * Set indicator is system setting.
     * @param system Indicator is system setting.
     */
    public void setSystem(final boolean system) {
        this.system = system;
    }
}
