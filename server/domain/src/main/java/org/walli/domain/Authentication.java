package org.walli.domain;

/** Enumeration of Authentication mechanisms which are currently implemented and who can be used for registering a Source provider. */
public enum Authentication {
    none("none"), rsa("rsa_sha1"), hmac("hmac_sha1"), basic("basic");

    /** The type. */
    private String type;

    /**
     * Constructor.
     * @param type The authentcation type.
     */
    private Authentication(final String type) {
        this.type = type;
    }

    /**
     * Gets the authentication type.
     * @return type The authentication type.
     */
    public String getType() {
        return type;
    }
}
