package org.walli.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;


/** The Project domain class represents a project and contains information about how to fetch data from sources for that project. */
@Entity
@Table(name = "Project", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@NamedQueries({
        @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p"),
        @NamedQuery(name = "Project.byId", query = "SELECT p FROM Project p where p.id = :id ")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(min = 1, max = 25)
    @Pattern(regexp = "[A-Za-z1-9_-]*", message = "This is not a valid name, it must contain only letters dashes and underscores")
    @Column(name = "name")
    private String name;

    @Column(name = "displayName")
    private String displayName;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "project", targetEntity = SourceInformation.class, fetch = FetchType.EAGER)
    private Set<SourceInformation> infos;

    /** Constructor. * */
    public Project() {
        infos = new HashSet<SourceInformation>();
    }

    /**
     * Gets the id.
     * @return id The id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the name.
     * @return name The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param name The name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the display name.
     * @return displayName The display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     * @param displayName The display name.
     */
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the set of source information's.
     * @return infos The source information's.
     */
    public Set<SourceInformation> getInfos() {
        return infos;
    }

    /**
     * Sets the set of source information's.
     * @param infos The source information's.
     */
    public void setInfos(final Set<SourceInformation> infos) {
        this.infos = infos;
    }
}
