package org.walli.di;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.walli.service.IProjectService;
import org.walli.service.ISourceService;
import org.walli.service.ProjectService;
import org.walli.service.SourceService;

import javax.inject.Inject;

import static org.junit.Assert.assertTrue;

/** Test class for {@link ServiceModule}. */
public class ServiceModuleTest {
    @Inject
    private ISourceService sourceService;

    @Inject
    private IProjectService projectService;

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new PersistenceModule(), new ServiceModule());
        injector.injectMembers(this);
    }

    @Test
    public void shouldInjectProjectService() throws Exception {
        assertTrue(projectService instanceof ProjectService);
    }

    @Test
    public void shouldInjectSourceService() throws Exception {
        assertTrue(sourceService instanceof SourceService);
    }

}
