package org.walli.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import org.junit.Before;
import org.junit.Test;
import org.walli.di.PersistenceModule;
import org.walli.di.ServiceModule;
import org.walli.domain.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.lang.reflect.Field;

import static org.junit.Assert.*;

/** Test class for {@link UserService} */
public class UserServiceTest {
    @Inject
    private UserService service; // class under test
    @Inject
    private Provider<EntityManager> provider;
    private EntityTransaction transaction;

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new PersistenceModule(), new ServiceModule());
        injector.getInstance(PersistService.class).start();
        injector.injectMembers(this);
        final EntityManager em = provider.get();
        transaction = em.getTransaction();

        final User user = new User();

        final Field username = user.getClass().getDeclaredField("username");
        username.setAccessible(true);
        username.set(user, "username");

        user.setFirstName("firstName");
        user.setLastName("lastName");
        user.setEmail("email@address.com");
        user.setPassword("password");
        user.setAdmin(true);

        transaction.begin();
        service.add(user);
        transaction.commit();
    }

    @Test
    public void shouldGetUsers() throws Exception {
        assertEquals(1, service.users().size());
    }

    @Test(expected = NoResultException.class)
    public void shouldThrowExceptionWhenGettingUserByIdNotFound() throws Exception {
        service.userById(0L);
    }

    @Test
    public void shouldGetUserById() throws Exception {
        final User user = service.userById(1L);
        assertEquals(1L, user.getId(), 0);
        assertEquals("username", user.getUsername());
        assertEquals("firstName", user.getFirstName());
        assertEquals("lastName", user.getLastName());
        assertEquals("email@address.com", user.getEmail());
        assertEquals("password", user.getPassword());
        assertTrue(user.isAdmin());
    }

    @Test
    public void shouldGetUserByName() throws Exception {
        final User user = service.userByUsername("username");
        assertEquals(1L, user.getId(), 0);
        assertEquals("username", user.getUsername());
        assertEquals("firstName", user.getFirstName());
        assertEquals("lastName", user.getLastName());
        assertEquals("email@address.com", user.getEmail());
        assertEquals("password", user.getPassword());
        assertTrue(user.isAdmin());
    }

    @Test
    public void shouldUpdateUser() throws Exception {
        User user = service.userById(1L);
        user.setFirstName("updatedFirstName");
        user.setLastName("updatedLastName");
        user.setEmail("updatedEmail@address.com");
        user.setPassword("updatedPassword");
        user.setAdmin(false);
        service.update(user);

        user = service.userById(1L);
        assertEquals(1L, user.getId(), 0);
        assertEquals("username", user.getUsername()); // cannot be updated. is unique.
        assertEquals("updatedFirstName", user.getFirstName());
        assertEquals("updatedLastName", user.getLastName());
        assertEquals("updatedEmail@address.com", user.getEmail());
        assertEquals("updatedPassword", user.getPassword());
        assertFalse(user.isAdmin());
    }

    @Test
    public void shouldDeleteUser() throws Exception {
        assertEquals(1, service.users().size());

        transaction.begin();
        service.delete(1L);
        transaction.commit();

        assertEquals(0, service.users().size());
    }
}
