package org.walli.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import org.junit.Before;
import org.junit.Test;
import org.walli.di.PersistenceModule;
import org.walli.di.ServiceModule;
import org.walli.domain.Configuration;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

import static org.junit.Assert.*;

/** Test class for {@link ConfigurationService} */
public class ConfigurationServiceTest {
    @Inject
    private IConfigurationService service; // class under test

    @Inject
    private Provider<EntityManager> provider;
    private EntityTransaction transaction;

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new PersistenceModule(), new ServiceModule());
        injector.getInstance(PersistService.class).start();
        injector.injectMembers(this);
        final EntityManager em = provider.get();
        transaction = em.getTransaction();

        final Configuration configuration = new Configuration();
        configuration.setName("name");
        configuration.setValue("value");
        configuration.setSystem(true);

        transaction.begin();
        service.add(configuration);
        transaction.commit();
    }

    @Test
    public void shouldGetConfigurations() {
        assertEquals(1, service.configurations().size());
    }

    @Test(expected = NoResultException.class)
    public void shouldThrowExceptionWhenGettingConfigurationByIdNotFound() throws Exception {
        service.configurationById(0L);
    }

    @Test
    public void shouldGetConfigurationById() throws Exception {
        final Configuration configuration = service.configurationById(1L);
        assertEquals(1L, configuration.getId(), 0);
        assertEquals("name", configuration.getName());
        assertEquals("value", configuration.getValue());
        assertTrue(configuration.isSystem());
    }

    @Test
    public void shouldUpdateConfiguration() throws Exception {
        Configuration configuration = service.configurationById(1L);
        configuration.setName("updatedName");
        configuration.setValue("updatedValue");
        configuration.setSystem(false);
        service.update(configuration);

        configuration = service.configurationById(1L);
        assertEquals("updatedName", configuration.getName());
        assertEquals("updatedValue", configuration.getValue());
        assertFalse(configuration.isSystem());
    }

    @Test
    public void shouldDeleteConfiguration() throws Exception {
        assertEquals(1, service.configurations().size());

        transaction.begin();
        service.delete(1L);
        transaction.commit();

        assertEquals(0, service.configurations().size());
    }
}
