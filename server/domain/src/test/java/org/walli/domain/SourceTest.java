package org.walli.domain;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/** Test class for {@link Source} */
public class SourceTest {

           @Test
        public void shouldSetValues() throws Exception {
            final Source source = new Source();
            final String privateKey = IOUtils.toString(SourceTest.class.getResourceAsStream("/myrsakey.pk8"));

            source.setName("name");
            source.setUrl("url");
            source.setAuthentication(Authentication.none);
            source.setCacheTime(100000);

            // RSA
            source.setPrivateKey(privateKey);

            // HMAC
            source.setConsumerSecret("consumerSecret");
            source.setOauthSecret("oauthSecret");

            // RSA / HMAC
            source.setConsumerKey("consumerKey");
            source.setOauthToken("oauthToken");

            // Basic
            source.setUsername("username");
            source.setPassword("password");

            assertEquals(0L, source.getId(), 0);
            assertEquals("name", source.getName());
            assertEquals("url", source.getUrl());
            assertEquals(100000, source.getCacheTime());
            assertEquals(Authentication.none, source.getAuthentication());

            // RSA
            assertEquals(privateKey, source.getPrivateKey());

            // HMAC
            assertEquals("consumerSecret", source.getConsumerSecret());
            assertEquals("oauthSecret", source.getOauthSecret());

            // RSA / HMAC
            assertEquals("consumerKey", source.getConsumerKey());
            assertEquals("oauthToken", source.getOauthToken());

            // Basic
            assertEquals("username", source.getUsername());
            assertEquals("password", source.getPassword());

    }
}
