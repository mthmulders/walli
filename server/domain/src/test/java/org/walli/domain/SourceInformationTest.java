package org.walli.domain;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/** Test class for {@link SourceInformation} */
public class SourceInformationTest {
    @Test
    public void shouldSetValues() throws Exception {
        final SourceInformation sourceInformation = new SourceInformation();
        final Project project = new Project();

        sourceInformation.setName("name");
        sourceInformation.setValue("value");
        sourceInformation.setProject(project);

        assertEquals(0L, sourceInformation.getId(), 0);
        assertEquals("name", sourceInformation.getName());
        assertEquals("value", sourceInformation.getValue());
        assertEquals(project, sourceInformation.getProject());
    }
}
