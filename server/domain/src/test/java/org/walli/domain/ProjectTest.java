package org.walli.domain;

import org.junit.Test;

import java.util.HashSet;

import static junit.framework.Assert.assertEquals;

/** Test class for {@link Project} */
public class ProjectTest {
    @Test
    public void shouldSetValues() throws Exception {
        final Project project = new Project();

        project.setName("name");
        project.setDisplayName("displayName");
        project.getInfos().add(new SourceInformation());

        assertEquals(0L, project.getId(), 0);
        assertEquals("name", project.getName());
        assertEquals("displayName", project.getDisplayName());
        assertEquals(1, project.getInfos().size());

        project.setInfos(new HashSet<SourceInformation>());
        assertEquals(0, project.getInfos().size());
    }
}