package org.walli.domain;

import junit.framework.Assert;
import org.junit.Test;

import static org.walli.domain.Authentication.valueOf;

/** Test class for {@link Authentication}. */
public class AuthenticationTest {

    @Test
    public void shouldGetCorrectEnumByType() throws Exception {
        Assert.assertEquals("none", valueOf("none").getType());
        Assert.assertEquals("rsa_sha1", valueOf("rsa").getType());
        Assert.assertEquals("hmac_sha1", valueOf("hmac").getType());
        Assert.assertEquals("basic", valueOf("basic").getType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenEnumNotFound() throws Exception {
        valueOf("nonexisting");
    }
}
